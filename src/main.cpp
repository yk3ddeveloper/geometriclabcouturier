#include <igl/readOFF.h>
#include <igl/avg_edge_length.h>
#include <igl/barycenter.h>
#include <igl/grad.h>
#include <igl/jet.h>
#include <igl/readDMAT.h>
#include <igl/readOBJ.h>
#include <igl/opengl/glfw/Viewer.h>
#include <igl/opengl/glfw/imgui/ImGuiMenu.h>
#include <igl/opengl/glfw/imgui/ImGuiHelpers.h>
#include <imgui/imgui.h>
#include <boost/program_options.hpp>
#include <iostream>
#include "PhysicalConst.h"
#include "geometry.h"
namespace po = boost::program_options;
int main(int argc, char *argv[])
{
  po::options_description desc("Choose Object to load");
  desc.add_options()
	  ("help", "produce help message")
	  ("avatar", po::value<std::string>(), "choose obj file to load")
	  ("max_gaussian", po::value<double>()->default_value(2*3.14), "choose obj file to load")
	  ("hide_menu", "hide menu if added")
	  ;
  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);
  if (vm.count("help")) {
	  std::cout << desc << "\n";
	  return 1;
  }
  Eigen::VectorXd K; // gaussian curvature test
  Eigen::MatrixXd C;
  Eigen::VectorXd H;
  Eigen::MatrixXd GU; 
  Eigen::VectorXd GU_mag;
  // Inline mesh of a cube
  Eigen::MatrixXd V= (Eigen::MatrixXd(8,3)<<
    0.0,0.0,0.0,
    0.0,0.0,1.0,
    0.0,1.0,0.0,
    0.0,1.0,1.0,
    1.0,0.0,0.0,
    1.0,0.0,1.0,
    1.0,1.0,0.0,
    1.0,1.0,1.0).finished();
  Eigen::MatrixXi F = (Eigen::MatrixXi(12,3)<<
    1,7,5,
    1,3,7,
    1,4,3,
    1,2,4,
    3,8,7,
    3,4,8,
    5,7,8,
    5,8,6,
    1,5,6,
    1,6,2,
    2,6,8,
    2,8,4).finished().array()-1;
  std::string record_colors = "cube";
  double max_gaussian = vm["max_gaussian"].as<double>();
  if (vm.count("avatar")) {
	  record_colors = vm["avatar"].as<std::string>();
	  Designer3D::loadModel(record_colors, V, F);
  } 
  Designer3D::processCurvatureDirection(V, F, H, C);
  Designer3D::processGaussian(V, F, K, C, max_gaussian);
  Designer3D::processGradient(V, F, GU, GU_mag);
  Eigen::MatrixXd V2 = V;
  Eigen::MatrixXi F2 = F;
  // Plot the mesh
  igl::opengl::glfw::Viewer viewer;



  igl::opengl::glfw::imgui::ImGuiMenu menu;
  // Attach a menu plugin
  if (vm.count("hide_menu")==0) {
	  
	  viewer.plugins.push_back(&menu);

	  float floatVariable = GRAVITY_ACCELERATION;
	  //menu.post_load = [&]() {
		 // //viewer.data().ver
		 // Designer3D::processCurvatureDirection(V, F, H, C);
		 // Designer3D::processGaussian(V, F, K, C, max_gaussian);
		 // Designer3D::processGradient(V, F, GU, GU_mag);
	  //};
	  menu.callback_draw_viewer_menu = [&]()
	  {
		  // Draw parent menu content
		  auto recalculate_all_data_and_draw = [&]() {
			  Designer3D::processCurvatureDirection(V2, F2, H, C);
			  Designer3D::processGaussian(V2, F2, K, C, max_gaussian);
			  Designer3D::processGradient(V2, F2, GU, GU_mag);
			  viewer.data().clear();
			  viewer.data().set_mesh(V2, F2);
			  viewer.data().show_lines = false;
			  viewer.data().set_colors(C);
		  };
		  //ImGui::SetNextWindowContentWidth(200.0f);
		  ImGui::Begin("Viewer2");
		  menu.draw_viewer_menu();
		  ImGui::SetWindowSize("Viewer2", ImVec2(350.0f, 1000.0f), 1);
		  ImGui::SetWindowPos("Viewer2", ImVec2(0.0f, 0.0f), 1);
		  // Add new group
		  if (ImGui::CollapsingHeader("New Group", ImGuiTreeNodeFlags_DefaultOpen))
		  {
			  if ( ImGui::InputText("Load Mesh", record_colors, ImGuiInputTextFlags_EnterReturnsTrue | ImGuiInputTextFlags_CallbackCompletion | ImGuiInputTextFlags_CallbackHistory, [](ImGuiTextEditCallbackData* data) ->int { return 1; }, (void*)NULL) ) {
			  
				  Designer3D::loadModel(record_colors, V, F);
				  V2 = V;
				  F2 = F;
				  recalculate_all_data_and_draw();
			  }
			  // Expose variable directly ...

			  // Add a button
			  if (ImGui::Button("Gaussian Curvature Coloring", ImVec2(-1, 0)))
			  {
				  igl::jet(K, true, C);
				  viewer.data().set_colors(C);
			  }
			  if (ImGui::Button("Direction Curvature Coloring", ImVec2(-1, 0)))
			  {
				  igl::parula(H, true, C);
				  viewer.data().set_colors(C);
			  }
			  if (ImGui::Button("Direction Gradient Coloring", ImVec2(-1, 0)))
			  {
				  igl::parula(GU, true, C);
				  viewer.data().set_colors(C);
			  }
			  if (ImGui::Button("Magnitude Gradient Coloring", ImVec2(-1, 0)))
			  {
				  igl::parula(GU_mag, true, C);
				  viewer.data().set_colors(C);
			  }
			  
			  if (ImGui::Button("Smooth Operation", ImVec2(-1, 0)))
			  {
				  recalculate_all_data_and_draw();
			  }
		  }
		  ImGui::End();
	  };
  }
  
  viewer.data().set_mesh(V2, F2);
  viewer.data().show_lines = false;
  viewer.data().set_colors(C);
  boost::filesystem::path base_path(argv[0], boost::filesystem::native);
  base_path = base_path.parent_path() / "logs" / "colors";
  if ( !boost::filesystem::exists(base_path) ) {
	  boost::filesystem::create_directory(base_path);
  }
  std::transform(record_colors.begin(), record_colors.end(), record_colors.begin(), [](const char c) -> char { return c == '/' || c == '\\' ? '_' : c; });
  boost::filesystem::ofstream colors_file(base_path / (record_colors + ".txt"));
  std::cout << "writing to " << base_path << std::endl;
  colors_file << "colors: " << C << std::endl;
  colors_file << "K: " << K << std::endl;
  colors_file << "H: " << H << std::endl;
  colors_file.close();
  //viewer.data().set_face_based(true);
  viewer.launch();
}
