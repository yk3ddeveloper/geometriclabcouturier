# - Try to find the LIBIGL library
# Once done this will define
#
#  LIBIGL_FOUND - system has LIBIGL
#  LIBIGL_INCLUDE_DIR - **the** LIBIGL include directory
if(CGAL_FOUND)
    return()
endif()

find_path(CGAL_INCLUDE_DIR CGAL/compiler_config.h
    HINTS
        ENV CGAL
        ENV CGALROOT
        ENV CGAL_ROOT
        ENV CGAL_DIR
    PATHS
        ${CMAKE_SOURCE_DIR}/../..
        ${CMAKE_SOURCE_DIR}/..
        ${CMAKE_SOURCE_DIR}
        ${CMAKE_SOURCE_DIR}/cgal
        ${CMAKE_SOURCE_DIR}/../cgal
        ${CMAKE_SOURCE_DIR}/../../cgal
        /usr
        /usr/local
        /usr/local/cgal
    PATH_SUFFIXES include
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(CGAL
    "\ncgal not found"
    CGAL_INCLUDE_DIR)
mark_as_advanced(CGAL_INCLUDE_DIR)
set(CGAL_DIR "${CGAL_INCLUDE_DIR}/..")
list(APPEND CMAKE_MODULE_PATH "${CGAL_INCLUDE_DIR}/..")
