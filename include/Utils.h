#pragma once

#include <boost/filesystem.hpp>
#define temp_wrapper_function(f) [](auto&&... args){ return f(std::forward<decltype(args)>(args)...); }
namespace filesys = boost::filesystem;

namespace Utils {
	typedef std::pair<bool, std::string> fileExtensionTest;
	fileExtensionTest getFileExtension(const std::string &filePath) {
		filesys::path pathObj(filePath);
		if (pathObj.has_extension()) {
			// Fetch the extension from path object and return
			return std::make_pair(true,pathObj.extension().string().substr(1));
		}
		return std::make_pair(false, std::string(""));
	}
}