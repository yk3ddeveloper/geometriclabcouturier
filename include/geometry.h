#pragma once
#include <igl/gaussian_curvature.h>
#include <igl/readOBJ.h>
#include <igl/cotmatrix.h>
#include <igl/avg_edge_length.h>
#include <igl/principal_curvature.h>
#include <igl/invert_diag.h>
#include <igl/barycenter.h>
#include <igl/grad.h>
#include <igl/jet.h>
#include <igl/readDMAT.h>
#include <igl/readOBJ.h>
#include <boost/algorithm/string.hpp>  
#include "Utils.h"
namespace Designer3D {
	std::pair<Eigen::Vector3d, Eigen::Vector3d> minMax(const Eigen::MatrixXd &V) {
		Eigen::Vector3d mi, ma;
		mi = V.colwise().minCoeff();
		ma = V.colwise().maxCoeff();
		return std::make_pair(mi, ma);
	}
	void canonize(Eigen::MatrixXd &V) {
		Eigen::Vector3d mi, ma;
		std::tie(mi, ma) = minMax(V);
		Eigen::Vector3d md = (mi + ma) / 2;
		std::cout << "Center found: " << md.transpose() <<"min: " << mi.transpose() << ",max: " << ma.transpose() << std::endl;
		V.rowwise() -= md.transpose();
		mi = mi - md;
		ma = ma - md;
		std::cout << "Fixed min: " << mi.transpose() << ",max: " << ma.transpose() << std::endl;
		V.array() /= ma.maxCoeff();
		//return V;
	}

	void processGaussian(const Eigen::MatrixXd &V, const Eigen::MatrixXi &F, Eigen::VectorXd &K, Eigen::MatrixXd &C,const double &maxxedGaussian) {
		
		Eigen::SparseMatrix<double> L,M, Minv;
		igl::gaussian_curvature(V, F, K);
		double kmean = K.mean();
		double std_deviation = (K - kmean * Eigen::VectorXd::Ones(K.size())).array().pow(2).sum() / K.size();
		std::cout << "K(" << K.minCoeff() << "," << K.maxCoeff() << ")mean:" << kmean << ",sd: " << std_deviation << std::endl;
		igl::massmatrix(V, F, igl::MASSMATRIX_TYPE_DEFAULT, M);
		igl::invert_diag(M, Minv);
		// Divide by area to get integral average
		K = (Minv*K).eval();
		K = K.array().max(-maxxedGaussian).min(maxxedGaussian);
		kmean = K.mean();
		std_deviation =  (K - kmean * Eigen::VectorXd::Ones(K.size())).array().pow(2).sum() / K.size();
		std::cout << "M*K(" << K.minCoeff() << "," << K.maxCoeff() << ")mean:" << kmean << ",sd: " << std_deviation << std::endl;
		igl::jet(K, true, C);
		std::cout << "C(" << C.colwise().minCoeff() << "," << C.colwise().maxCoeff() << ")" << std::endl;

	}
	void processCurvatureDirection(const Eigen::MatrixXd &V, const Eigen::MatrixXi &F, Eigen::VectorXd &H, Eigen::MatrixXd &C) {
		Eigen::MatrixXd HN;
		Eigen::SparseMatrix<double> L, M, Minv;
		igl::cotmatrix(V, F, L);
		igl::massmatrix(V, F, igl::MASSMATRIX_TYPE_VORONOI, M);
		igl::invert_diag(M, Minv);
		// Laplace-Beltrami of position
		HN = -Minv * (L*V);
		// Extract magnitude as mean curvature
		H = HN.rowwise().norm();
		Eigen::MatrixXd PD1, PD2;
		Eigen::VectorXd PV1, PV2;
		igl::principal_curvature(V, F, PD1, PD2, PV1, PV2);
		// mean curvature
		H = 0.5*(PV1 + PV2);
		igl::parula(H, true, C);
	}
	void processGradient(const Eigen::MatrixXd &V, const Eigen::MatrixXi &F, Eigen::MatrixXd &GU, Eigen::VectorXd &GU_mag) {
		Eigen::SparseMatrix<double> Gd;
		igl::grad(V, F, Gd);
		Eigen::VectorXd U = Eigen::VectorXd::Ones(V.rows());
		GU = Eigen::Map<const Eigen::MatrixXd>((Gd*U).eval().data(), F.rows(), 3);
		GU_mag = GU.rowwise().norm();
	}

	void loadModel(const std::string &modelName, Eigen::MatrixXd &V, Eigen::MatrixXi &F) {
		std::string baseName = std::string(ASSETS_PATH) + "/models/" + modelName;
		Utils::fileExtensionTest fileExtension = Utils::getFileExtension(modelName);
		std::map<std::string, std::function<bool(std::string&, Eigen::PlainObjectBase<Eigen::MatrixXd> &, Eigen::PlainObjectBase<Eigen::MatrixXi> &)> > readFiles;
		readFiles["obj"] = temp_wrapper_function(igl::readOBJ);
		readFiles["off"] = temp_wrapper_function(igl::readOFF);
		if (!fileExtension.first) {
			// test extensions our selves
			std::string objName;
			for (auto &ty : readFiles) {
				objName = baseName + "." + ty.first;
				if (ty.second(objName, V, F)) {
					break;
				}
			}
		}
		else {
			std::string extension_lower = fileExtension.second;
			boost::algorithm::to_lower(extension_lower);
			std::cout << "extension found " << extension_lower << std::endl;
			if (readFiles.count(extension_lower)) {
				auto readFunction= readFiles[extension_lower];
				readFunction(baseName, V, F);
			}
		}
		canonize(V);
	}
	std::pair<Eigen::MatrixXd, Eigen::MatrixXi> boundingBox(const std::pair<Eigen::Vector3d, Eigen::Vector3d> &boundaries) {
		Eigen::MatrixXi E_box(12, 2);
		E_box <<
			0, 1,
			1, 2,
			2, 3,
			3, 0,
			4, 5,
			5, 6,
			6, 7,
			7, 4,
			0, 4,
			1, 5,
			2, 6,
			7, 3;
		Eigen::MatrixXd V_box(8, 3);
		Eigen::Vector3d m, M;
		std::tie(m, M) = boundaries;
		V_box <<
			m(0), m(1), m(2),
			M(0), m(1), m(2),
			M(0), M(1), m(2),
			m(0), M(1), m(2),
			m(0), m(1), M(2),
			M(0), m(1), M(2),
			M(0), M(1), M(2),
			m(0), M(1), M(2);
		return std::make_pair(V_box, E_box);
	}
}