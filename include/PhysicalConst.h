#pragma once
// all physical constants, using cgs system

#define GRAVITY_ACCELERATION		980.0f		// gravity acceleration cm/s^2
// time units
#define TIME_STEP					0.0003f		// seconds
#define TIME_STEP_MAXIMUM			0.01f		// seconds
#define TIME_STEP_MINIMUM			0.0001f		// seconds
#define TIME_STEP_RESOLUTION		0.0001f		// seconds


// Wind force size in gr*cm/s^2
#define WIND_FORCE_AMP					0.0f		// gr*cm/s^2
#define WIND_FORCE_AMP_MAXIMUM			600.0f		// gr*cm/s^2
#define WIND_FORCE_AMP_MINIMUM			-600.0f		// gr*cm/s^2
#define WIND_FORCE_AMP_RESOLUTION		0.1f		// gr*cm/s^2

// Wind force size in gr*cm/s^2
#define WIND_FORCE_AMP_MODIFIER					0.5f		// gr*cm/s^2
#define WIND_FORCE_AMP_MODIFIER_MAXIMUM			5.0f		// gr*cm/s^2
#define WIND_FORCE_AMP_MODIFIER_MINIMUM			-5.0f		// gr*cm/s^2
#define WIND_FORCE_AMP_MODIFIER_RESOLUTION		0.01f		// gr*cm/s^2


// Wind force size in gr*cm/s^2
#define WIND_SPATIAL_DIVISION_FACTOR			0.1f		// 
#define WIND_SPATIAL_DIVISION_FACTOR_MAXIMUM	1.0f		// 
#define WIND_SPATIAL_DIVISION_FACTOR_MINIMUM	0.002f		// 
#define WIND_SPATIAL_DIVISION_FACTOR_RESOLUTION		0.002f		// 

#define WIND_FORCE_CONTROL_POINTS_OFFSET_SINGLE_BOUND 2
#define WIND_FORCE_CONTROL_POINTS_OFFSET (2u*WIND_FORCE_CONTROL_POINTS_OFFSET_SINGLE_BOUND)
#define WIND_FORCE_CONTROL_POINTS_ROWS		100
#define WIND_FORCE_CONTROL_POINTS_COLUMNS		100

#define WIND_FORCE_CONTROL_POINTS_ROWS_MAXIMUM		(500+WIND_FORCE_CONTROL_POINTS_OFFSET)
#define WIND_FORCE_CONTROL_POINTS_COLUMNS_MAXIMUM		(500+WIND_FORCE_CONTROL_POINTS_OFFSET)
#define WIND_FORCE_CONTROL_POINTS_ROWS_MINIMUM		10
#define WIND_FORCE_CONTROL_POINTS_COLUMNS_MINIMUM		10

// steps per frame, dimensionless, number of steps before rendering
#define STEPS_FACTOR					10u
#define STEPS_PER_FRAME					(5u*STEPS_FACTOR)		// 
#define STEPS_PER_FRAME_MAXIMUM			(30u*STEPS_FACTOR)		// 
#define STEPS_PER_FRAME_MINIMUM			1u		// 
#define STEPS_PER_FRAME_RESOLUTION		1u		// 

// Position correction coefficent, currently based on tests, dimensionless
#define POSITION_CORRECTION_COEFFICENT					0.01f		// 
#define POSITION_CORRECTION_COEFFICENT_MAXIMUM			1.0f		// 
#define POSITION_CORRECTION_COEFFICENT_MINIMUM			0.001f		// 
#define POSITION_CORRECTION_COEFFICENT_RESOLUTION		0.001f		// 

// mass concentration, might be changed per garment giving a range, taken from http://graphics.berkeley.edu/papers/Wang-DDE-2011-08/material_parameters.pdf
// note however that this paper work on mks system , not cgs, values should be multiplied by 10 because 1 kg/m^2 = 0.1 gr/cm^2
#define PARTICLE_MASS_DENSITY				0.0324f		// mass density of particle  gr/cm^2, default value took for 99% cotton, 1% spandex jeans
#define PARTICLE_MASS_DENSITY_MINIMUM		0.01f		// minimum mass density of particle  gr/cm^2
#define PARTICLE_MASS_DENSITY_MAXIMUM		0.0324f		// maximum mass density of particle  gr/cm^2
#define PARTICLE_MASS_DENSITY_RESOLUTION	0.0002f		// mass density of particle control resolution gr/cm^2



// air drag force coefficent factor for Stokes Drag useful for low speeds, which is anticipated.
// Stokes Drag is F_d=-b*v, Air drag force factor is the b, might change between fabrics, 
// roAir = 0.12 gr/cm^3
// tempreture is 18 degree celsius so mu_air == mu_0_air, look at kinematic viscosity at https://en.wikipedia.org/wiki/Viscosity#Kinematic_viscosity
// mu_0 air is 18.27 micro Pa/sec = 182.7 micro dyn/(cm^2*sec)
// will currently use default 1000 gr/s from ido tests but need to be researched further, at paper https://arxiv.org/ftp/arxiv/papers/1509/1509.07542.pdf
#define AIR_DRAG_FORCE_FACTOR								1000.0f //gr/s
#define AIR_DRAG_FORCE_FACTOR_MINIMUM						100.0f //gr/s
#define AIR_DRAG_FORCE_FACTOR_MAXIMUM						10000.0f //gr/s
#define AIR_DRAG_FORCE_FACTOR_RESOLUTION					100.0f //gr/s


// avatar drag force coefficent currently based on from fabric http://www.mdpi.com/2075-4442/4/1/6/pdf, dimensionless
#define AVATAR_FRICTION_FORCE_FACTOR								0.5f 
#define AVATAR_FRICTION_FORCE_FACTOR_MINIMUM						0.01f
#define AVATAR_FRICTION_FORCE_FACTOR_MAXIMUM						0.8f
#define AVATAR_FRICTION_FORCE_FACTOR_RESOLUTION						0.01f

// Rest length dimensionless correction factor to test energetic fabric, 1.0 is non energetic
#define REST_LENGTH_CORRECTION_FACTOR								1.0f 
#define REST_LENGTH_CORRECTION_FACTOR_MINIMUM						0.5f
#define REST_LENGTH_CORRECTION_FACTOR_MAXIMUM						1.5f
#define REST_LENGTH_CORRECTION_FACTOR_RESOLUTION					0.01f

// K stretch,bend and shear will change between garment giving ranges for testing
// values calculated from http://www.gmrv.es/~gcirio/pdf/YarnLevelWovenCloth_SIGASIA2014.pdf
// area of contact for Yarn with radius of 0.25mm is 1.9625*1e-3 cm^2 == 0.19625 mm^2
// Bending modulus is B = 1e-2 Pa == 0.1 dyn/cm^2
// therefore default stiffness density == 1.9625*1e-4 dyn/cm, this sytem does not fit for use,
//  bending force is too small, note the equation at page 4, depends on theta between yarns
// note to myself look at theta def at fig 5-b and combine with eq (8) at section 4.2 
// stretch based on calculations in page 4
// Stretch force is more reliable elstic elasticmode Y = 1e7 Pa = 1e8 dyn/cm^2
// default stiffness is therefore 1.9625*1e5 dyn/cm and will multiplied by half to get 9.8125*1e4
// this is unfit
#define K_FACTOR					10.0f  // to translate from kgm to grcm
#define K_BEND						(500.0f*K_FACTOR)	// k bend gr/s^2
#define K_STRETCH					(1000.0f*K_FACTOR)	// k stiffness gr/(s^2), 500 (with factor 10 to 5k) chosen by tests on plane
#define K_SHEAR						(50.0f*K_FACTOR)	// k shear gr/s^2, currently disabled
#define K_MINIMUM_BEND				(50.0f*K_FACTOR)		// k minimum bend gr/s^2
#define K_MAXIMUM_BEND				(5000.0f*K_FACTOR)	// k maximum bend gr/s^2

#define K_MINIMUM_STRETCH			(50.0f*K_FACTOR)		// k minimum semi stiffness gr/(cm*s^2)
#define K_MAXIMUM_STRTECH			(5000.0f*K_FACTOR)	// k maximum semi stiffness gr/(cm*s^2)

#define K_MINIMUM_SHEAR				(50.0f*K_FACTOR)		// k minimum shear gr/s^2
#define K_MAXIMUM_SHEAR				(5000.0f*K_FACTOR)	// k maximum shear gr/s^2

#define K_STEP						(1.0f*K_FACTOR)		// k step alignments

#define DISTANCE_UNIT				1.0f		// distance of  scalar 1 is 1 cm