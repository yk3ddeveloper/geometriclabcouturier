#define COUTURIER_GPU
#include "CpuGpuSharedDataStructures.h"
#include "PhysicalConst.h"
#define EPSILON 0.000001f
#define INVEPSILON 1.0e6f
//
// calc forces flat mode
//
__kernel void calc_forces_nvidia(__global float4* positions,
	__global float4* forcesVectors,
	__global cd_index_type* forceParticle1,
	__global cd_index_type* forceParticle2,
	__global cd_physical_scalar* forceRestLength,
	__global cd_physical_scalar* forceKStretch,
	__global cd_physical_scalar* forceKBend,
	__global cd_physical_scalar* forceKShear,
	Params params) {

	cd_index_type gid = get_global_id(0);
	cd_index_type fParticle1 = forceParticle1[gid];
	cd_index_type fParticle2 = forceParticle2[gid];
	cd_vec4 point1 = positions[fParticle1];
	cd_vec4 point2 = positions[fParticle2];
	float4 diff = point2 - point1; 
	float3 diff3 = diff.xyz;
	diff.w = 0.0f;

	cd_physical_scalar ldf = length(diff3);
	diff3 = normalize(diff3);
	cd_physical_scalar fRestLength = forceRestLength[gid];
	fRestLength = fRestLength *params.RestLengthCorrectionFactor;
	barrier(CLK_GLOBAL_MEM_FENCE);
	cd_physical_scalar dx = ldf - fRestLength; //  calculate changed length dx
	cd_physical_scalar kSt = params.kStretch;
	cd_physical_scalar kBe = params.kBend;
	cd_physical_scalar kSh = params.kShear;
	cd_physical_scalar kStf = forceKStretch[gid];
	if (params.StretchActive == 0) {
		kStf = 0.0f;
		kSh = 0.0f;
	}
	cd_physical_scalar kBef = forceKBend[gid];
	if (params.BendActive == 0) {
		kBef = 0.0f;
	}
	cd_physical_scalar kShf = forceKShear[gid];
	cd_physical_scalar k = kSt*kStf +kBe*kBef + kSh*kShf; // calculate linear factor for spring
	float4 calculatedForce = (float4)(0.0f,0.0f,0.0f,1.0f);
	float3 calculatedForce3 = (float3)(0.0f, 0.0f, 0.0f);
	if (k > EPSILON) {
		calculatedForce3.x = clamp(diff3.x, -1.0f, 1.0f)*dx*k; // calculate spring force
		calculatedForce3.y = clamp(diff3.y, -1.0f, 1.0f)*dx*k; // calculate spring force
		calculatedForce3.z = clamp(diff3.z, -1.0f, 1.0f)*dx*k; // calculate spring force
	}
	barrier(CLK_GLOBAL_MEM_FENCE);
	//if (fParticle1 == 1975 || fParticle2 == 1975) {
	//	//printf("gid(%u)=p1(%u)(x:%6.3f,y:%6.3f,z:%6.3f,w:%6.3f),p2(%u)(x:%6.3f,y:%6.3f,z:%6.3f,w:%6.3f)\n", gid, fParticle1, point1.x, point1.y, point1.z, point1.w, fParticle2, point2.x, point2.y, point2.z, point2.w);
	//	printf("gid(%u)=diff(x:%6.3e,y:%6.3e,z:%6.3e),dx=%6.3e,k=%6.3e\n", gid, diff3.x, diff3.y, diff3.z,dx,k);
	//	//printf("gid(%u,1:%d,2:%d):cf=(x:%6.3e,y:%6.3e,z:%6.3e),dx=%6.3e,k=%6.3e,dxk=%6.3e\n", gid, fParticle1, fParticle2, calculatedForce3.x, calculatedForce3.y, calculatedForce3.z, dx, k,dx*k);
	//}
	calculatedForce.xyz = calculatedForce3;
	//calculatedForce.w = 0.0f;
	forcesVectors[gid] = calculatedForce;
}
__kernel void calc_forces_points(__global float4* positions,
	__global cd_index_type* forceParticle1,
	__global cd_index_type* forceParticle2,
	__global float4* forcesVectors,
	__global float4* forcesPoints,
	Params params) {
	cd_index_type gid = get_global_id(0); 
	cd_index_type fParticle1 = forceParticle1[gid];
	cd_index_type fParticle2 = forceParticle2[gid];
	cd_index_type ogid = gid << 1;
	cd_vec4 point1 = positions[fParticle1];
	cd_vec4 point2 = positions[fParticle2];
	forcesPoints[ogid] = point1;
	forcesPoints[ogid+1] = point2;
}

bool isPositionInsideAabb(__global float4* aabb_boxes, cd_index_type boxIndex, float3 position) {
	cd_index_type boundIndex = boxIndex << 1;
	float4 minBound = aabb_boxes[boundIndex];
	float4 maxBound = aabb_boxes[boundIndex +1];
	return (position.x > minBound.x &&
		position.x < maxBound.x &&
		position.y > minBound.y &&
		position.y < maxBound.y &&
		position.z > minBound.z &&
		position.z < maxBound.z);
}

cd_index_type findDeepestContainingAabb(__global cd_index_type* aabb_features,__global float4* aabb_boxes, float3 position,cd_index_type start_index) {

	
	if (!isPositionInsideAabb(aabb_boxes, start_index, position))
		return start_index;
	cd_index_type pointIndex = start_index;
	cd_index_type featureIndex = pointIndex << 2;
	cd_index_type left = aabb_features[featureIndex];
	cd_index_type right = aabb_features[featureIndex+1];
	while (left > 0 && right > 0) {
		if (isPositionInsideAabb(aabb_boxes, left, position)) {
			pointIndex = left;
		}
		else if (isPositionInsideAabb(aabb_boxes, right, position)) {
			pointIndex = right;
		}
		else
			break;
		featureIndex = pointIndex << 2;
		left = aabb_features[featureIndex];
		right = aabb_features[featureIndex + 1];
	}

	return pointIndex;
}

uint4 findDeepestContainingAabbfindBoth(__global cd_index_type* aabb_features, __global float4* aabb_boxes, float3 position,float3 startPosition) {


	uint4 pointIndex = (uint4)(0, 0, 0, 0);
	if (!isPositionInsideAabb(aabb_boxes, 0, position))
		return pointIndex;
	
	cd_index_type featureIndex = pointIndex.x << 2;
	cd_index_type left = aabb_features[featureIndex];
	cd_index_type right = aabb_features[featureIndex + 1];
	while (left > 0 && right > 0) {
		if (isPositionInsideAabb(aabb_boxes, left, position)) {
			pointIndex.x = left;
			if (isPositionInsideAabb(aabb_boxes, left, startPosition)) {
				pointIndex.y = left;

			}
		}
		else if (isPositionInsideAabb(aabb_boxes, right, position)) {
			pointIndex.x = right;
			if (isPositionInsideAabb(aabb_boxes, right, startPosition)) {
				pointIndex.y = right;

			}
		}
		else
			break;
		featureIndex = pointIndex.x << 2;
		left = aabb_features[featureIndex];
		right = aabb_features[featureIndex + 1];
	}

	return pointIndex;
}

bool isVertexBehindOrientedTriangle(float4 v0,float4 v1,float4 v2, float3 vertex) {
	float3 U = (v1 - v0).xyz;
	float3 V = (v2 - v0).xyz;

	float3 v03 = v0.xyz;

	float uu = dot(U, U);
	float vv = dot(V, V);

	float a = dot(vertex, U);
	float b = dot(v03, U);
	float c = dot(V, U);
	//float abc = dot(vertex - v03 - V, U);
	float d = dot(vertex, V);
	float e = dot(v03, V);
	//float de = dot(vertex -v03, V);
	float t = (d - e) /vv - c*(a -b -c) /(uu*vv);
	float s = (a - b - t*c) / uu;
	float trianglesFix = 0.01f;// EPSILON;

	return (((t + s) < 1.0f - trianglesFix) && (s > trianglesFix) && (t > trianglesFix));

}



// test traingle intersection and returns correct intersection point if exists
// from http://www.lighthouse3d.com/tutorials/maths/ray-triangle-intersection/
float4 IntersectRayTriangle(float4 v0, float4 v1, float4 v2, float3 p,float3 d) {
	float3 e2 = (v2 - v0).xyz;
	float3 e1 = (v1 - v0).xyz;
	float3 h = cross(d, e2);
	float a = dot(e1, h);
	float4 result = (float4)(0.0f, 0.0f, 0.0f, 0.0f);
	if (a > -EPSILON && a < EPSILON)
		return result;

	float f = 1.0f / a;
	float3 s =  p.xyz - v0.xyz;
	float u = f * dot(s, h);

	if (u < 0.0f || u > 1.0f)
		return result;
	float3 q = cross(s, e1);
	
	float v = f * dot(d, q);

	if (v < 0.0f || u + v > 1.0f)
		return result;

	// at this stage we can compute t to find out where
	// the intersection point is on the line
	float t = f * dot(e2, q);
// ray intersection
	result.x = t;
	result.w = 1.0f;
	return result;
}


// kernel run once per force
__kernel void update_forces_features(__global cd_vec4* positions,
	__global cd_vec4* particlePreviousPosition,
	__global cd_vec4* forcesVectors,
	__global cd_vec4* forceFeatures,
	__global cd_index_type* forceParticle1,
	__global cd_index_type* forceParticle2,
	__global cd_physical_scalar* forceRestLength,
	__global cd_physical_scalar* forceKStretch,
	__global cd_physical_scalar* forceKBend,
	__global cd_physical_scalar* forceKShear,
	Params params) {
	// load data to the registers
	cd_index_type gid = get_global_id(0);
	cd_index_type ogid = gid << 1;
	cd_index_type tail_id = forceParticle1[gid];
	cd_index_type head_id = forceParticle2[gid];
	cd_physical_scalar rest_length = forceRestLength[gid];
	// for force composition
	cd_physical_scalar kStf = forceKStretch[gid];
	cd_physical_scalar kBef = forceKBend[gid];
	cd_physical_scalar kShf = forceKShear[gid];
	// particles new position
	cd_vec4 tail_position = positions[tail_id];
	cd_vec4 head_position = positions[head_id];
	// particle old position
	cd_vec4 previous_tail_position = positions[tail_id];
	cd_vec4 previous_head_position = positions[head_id];
	cd_vec4 result = (cd_vec4)(0.5f, 0.5f, 0.5f, 0.5f);
	cd_physical_scalar current_length = distance(head_position.xyz, tail_position.xyz);
	cd_physical_scalar previous_length = distance(previous_head_position.xyz, previous_tail_position.xyz);
	cd_physical_scalar grey_factor = (current_length - 0.5f*previous_length) / current_length;
	result.x = (current_length - previous_length)/ rest_length; // compression factor
	result.y = current_length / rest_length; // compression status
	result.z = current_length; // current length for propriety
	//result.w = previous_length; // moving average to test difference over time
	float forceUp = head_position.y > tail_position.y ? 1.0f : 0.0f;
	result.w = kStf*1.0f + kBef*2.0f + kShf*4.0f+ forceUp*8.0f;
	//result.xyz = result.xyz * clamp(grey_factor, 0.0f, 1.0f);
	// save forces features
	forceFeatures[ogid] = result;
	forceFeatures[ogid+1] = result;
}



//
// update particles flat mode
//
__kernel void update_particles_nvidia_pre_update(__global float4* positions,
	__global float4* particlePreviousPosition,
	__global float4* forcesVectors,
	__global cd_physical_scalar* particleInvArea,
	__global cd_index_type* particleForcesIndices,
	__global cd_index_type* particleForcesPerParticle,
	__global cd_index_type* aAabbfeatures,
	__global cd_index_type* aAabbLeafsTrianglesIndices,
	__global cd_vec4* aAabbBoxes,
	__global cd_vec4* pAvatarTriangles,
	__global cd_physical_scalar* particleForcesCoefficents,
	__global cd_bool* particleFixed,
	Params params)
{
	cd_index_type gid = get_global_id(0);
	if (particleFixed[gid] == 1) {
		return;
	}
	const cd_physical_scalar dtsquare = params.dtsquare;
	const cd_physical_scalar dt = params.dt;
	const cd_physical_scalar invdt = params.invdt;
	float4 position = positions[gid];
	float4 pposition = particlePreviousPosition[gid];
	float4 WindForce = params.WindForceVec;
	float WindForceAmp = params.WindForceAmplitude;
	WindForce = WindForce*WindForceAmp;
	float4 GravityAcc = params.GravityAccVec;
	float3 gravity = GravityAcc.xyz;
	float4 MouseForce = params.vecMouseForce;
	float3 velocity = (position.xyz - pposition.xyz) * invdt;
	if (position.y < EPSILON*-1.0f && params.FloorActive == 1)
		velocity.y = 0.0f;
	float3 positionCorrection = (float3)(0.0f, 0.0f, 0.0f);
	float3 frictionForce = (float3)(0.0f, 0.0f, 0.0f);
	float4 ForceSum = (float4)(0.0f, 0.0f, 0.0f,0.0f);
	float3 normalx = (float3)(0.0f, 0.0f, 0.0f);
	cd_index_type aabb_container_id = findDeepestContainingAabb(aAabbfeatures, aAabbBoxes, position.xyz,0);
	cd_index_type feature_id = aabb_container_id << 2;
	cd_index_type triangles_for_aabb = aAabbfeatures[feature_id + 2];
	bool found_intersection = false;
	if (triangles_for_aabb > 0 && feature_id > 0 && params.CollisionActive == 1) {
		cd_index_type leaf_index = aAabbfeatures[feature_id + 3];
		cd_index_type trianglesIndicesOffset = AABB_MAX_TRIANGLES_DEF*leaf_index;
		cd_index_type i = 0;
		
		while ( i<triangles_for_aabb && !found_intersection) {
			cd_index_type triangleFeatureIndex = aAabbLeafsTrianglesIndices[trianglesIndicesOffset+i];
			triangleFeatureIndex = triangleFeatureIndex << 2;
			cd_vec4 v0 = pAvatarTriangles[triangleFeatureIndex];
			cd_vec4 v1 = pAvatarTriangles[triangleFeatureIndex+1];
			cd_vec4 v2 = pAvatarTriangles[triangleFeatureIndex+2];
			cd_vec4 nrml = pAvatarTriangles[triangleFeatureIndex + 3];
			if (isVertexBehindOrientedTriangle(v0,v1,v2, position.xyz)) {
				//if(didParticleCrossTriangleBetweenSteps(triangles[triangleIndex], particles[gid].pos, particles[gid].ppos)){
				normalx = nrml.xyz;

				float3 perpV = normalx*dot(normalx, velocity);
				velocity -= perpV;
				//frictionForce = normalize(velocity)*-1.0f * params.AvatarFritionCoefficient;
				float3 movement = pposition.xyz - position.xyz;
				float3 fixVector = normalx * dot(movement, normalx); // previous movement into the triangle that need to be fixed
				positionCorrection = normalx * params.PositionCorrectionCoefficient;//direction*0.3f;
				float3 gravity = (float3)(0.0f, 0.0f, 0.0f);

				found_intersection = true;
			}
			i++;
		}
	}
	cd_physical_scalar invArea = particleInvArea[gid];
	cd_index_type start_index_coefficent = MAX_FORCES_PER_PARTICLE*gid;
	cd_index_type end_index_coefficent = start_index_coefficent + particleForcesPerParticle[gid];
	float pInvMass = params.invParticleMass* invArea;
	for (cd_index_type force_id = start_index_coefficent; force_id < end_index_coefficent; force_id++) {
		cd_index_type force_index = particleForcesIndices[force_id];
		cd_physical_scalar coeff = particleForcesCoefficents[force_id];
		float4 forceVector = forcesVectors[force_index];
		ForceSum = ForceSum + coeff * forceVector;
	}

	float3 airDragForce = velocity*-1.0f* params.AirDragFroceCoefficient;
	// note for total force on the particle we need to include the gravity force too...
	float3 netForce = ForceSum.xyz; // + airDragForce + WindForce.xyz
	if (params.GravityActive == 1) {
		netForce = netForce + (gravity / pInvMass);
	}
	if (found_intersection) {

		// friction force negate normal * net force for calculations
		float sinalphaForNormal = 1 - dot(normalize(netForce), -normalx);
		sinalphaForNormal = sinalphaForNormal * (-1.0f) * length(netForce) *params.AvatarFritionCoefficient;
		frictionForce = frictionForce + normalize(netForce) * sinalphaForNormal;
	}
	netForce = netForce + frictionForce;
	
	float3 acceleration =  netForce * pInvMass;
	float4 newPosition = (float4)(0.0f, 0.0f, 0.0f, 1.0f);
	//
	//position.xyz = position.xyz + positionCorrection;
	newPosition.xyz = position.xyz + velocity*dt + acceleration*dtsquare;
	newPosition.xyz = newPosition.xyz + positionCorrection;
	
	particlePreviousPosition[gid] = position;
	positions[gid] = newPosition;
}

float sinthethaFromCosTheta(float costheta) {
	return sqrt(1.0f - costheta*costheta);
}

//
//  IntersectTriangle
// Fast  Minimum Storage Ray Triangle Intersection
float4 IntersectTriangleLocal(float4 v0,float4 v1,float4 v2,float4 RayEnd,float4 RayStart,bool test_cull)
{

	
	float fT = 0.0f;
	float fU = 0.0f;
	float fV = 0.0f;
	float3 Vertex1 = v0.xyz;
	float3 Vertex2 = v1.xyz;
	float3 Vertex3 = v2.xyz;
	float4 result = (float4)(0.0f, 0.0f, 0.0f, -1.0f);
	// Find vectors for two edges sharing vert1
	float3 Edge1 = Vertex2 - Vertex1;
	float3 Edge2 = Vertex3 - Vertex1;

	float3 RayDirection = RayEnd.xyz - RayStart.xyz;
	float3 RayDirNormalize = normalize(RayDirection.xyz);

	// Begin calculating determinant - alos used to calculate U parameter
	float3 VecCross = cross(RayDirNormalize, Edge2);
	float3 vecCrossVParam = (float3)(0.0f, 0.0f, 0.0f);
	float3 DisVert1RayOrigin = (float3)(0.0f, 0.0f, 0.0f);
	// If determinant is near zero, ray lies in plane of triangle
	//float fDet = (float)dot((float3)Edge1, float3(VecCrossNarrow));
	float fDet = dot(Edge1, VecCross);

	if ( test_cull ) {
		if (fDet >= EPSILON) {
			// Calculate distance from vert1 to ray origin
			DisVert1RayOrigin = RayStart.xyz - Vertex1;

			// calculate U parameter and test boun
			fU = dot(DisVert1RayOrigin, VecCross);
			if (fU >= 0.0f && fU <= fDet) {
				// Prepare to test V parameter
				vecCrossVParam = cross(DisVert1RayOrigin, Edge1);
				fV = dot(RayDirNormalize, vecCrossVParam);
				if (fV >= 0.0f && fU + fV <= fDet ) {
					fT = dot(Edge2, vecCrossVParam) / fDet;
					result.w = fT;
					result.xyz = RayStart.xyz + RayDirNormalize.xyz*fT;
				}
			}
		}

	} else if ((-EPSILON >= fDet) || (EPSILON <= fDet)) {
		float InvDet = 1.0f / fDet;
		

		// Calculate distance from vert1 to ray origin
		DisVert1RayOrigin = RayStart.xyz - Vertex1;

		// Calculate U parameter and test bounds
		fU = dot(DisVert1RayOrigin, VecCross) * InvDet;

		if ((0.0f <= fU) && (1.0f >= fU)) {

			// Prepare to test V parameter
			
			vecCrossVParam = cross(DisVert1RayOrigin, Edge1);
			//Calculate V parameter and test bounds
			fV = dot(RayDirNormalize, vecCrossVParam) * InvDet;

			if ((0.0f <= fV) && (1.0f >= fU + fV)) {
				// Calculate t, ray intersects triangle
				fT = dot(Edge2, vecCrossVParam) *InvDet;
				result.w = fT;
				result.xyz = RayStart.xyz + RayDirection.xyz*fT;
			}
		}
	}
	return result;
}

//
// update particles flat mode, test changed physics
//
__kernel void reset_particles_features(__global float4* positions,
	__global float4* particlePreviousPosition,
	__global float4* pointsPentrationsVectors,
	__global float4* pointsCorrectionsVectors,
	__global cd_vec4* generalPurposeParticleBuffer7, // for tests
	__global cd_vec4* generalPurposeParticleBuffer8, // for tests
	__global cd_vec4* generalPurposeParticleBuffer8p2, // for tests
	__global cd_vec4* generalPurposeParticleBuffer9, // for tests
	__global cd_bool* particleFixed,
	__global cd_bool* particleFixedBackup,
	__global int             *pointsMarked,
	__global float4* forcesVectors,
	__global float4* pointsFeatures) {

	cd_index_type gid = get_global_id(0);
	particleFixed[gid] = particleFixedBackup[gid];
	cd_index_type penetrationIndexForVertex = gid << 2;
	float4 features = (float4)(0.0f, 0.0f, 0.0f, 0.0f);
	float4 featuresBlack = features;
	featuresBlack.w = 1.0f;
	generalPurposeParticleBuffer9[gid] = featuresBlack;
	generalPurposeParticleBuffer8[gid] = featuresBlack;
	generalPurposeParticleBuffer8p2[gid << 1] = featuresBlack;
	generalPurposeParticleBuffer8p2[1+(gid << 1)] = featuresBlack;
	generalPurposeParticleBuffer7[gid] = featuresBlack;
	pointsFeatures[gid] = features;
	float4 startColor = (float4)(0.0f, 0.0f, 0.0f, 0.0f);
	float4 startPosition = (float4)(0.0f, 0.0f, 0.0f, 0.0f);
	for (int point_order_in_stride = 0; point_order_in_stride < 2; point_order_in_stride++) {
		pointsPentrationsVectors[penetrationIndexForVertex + (point_order_in_stride<<1)] = startPosition;
		pointsPentrationsVectors[penetrationIndexForVertex + (point_order_in_stride<<1) + 1] = startColor;
		pointsCorrectionsVectors[penetrationIndexForVertex + (point_order_in_stride << 1)] = startPosition;
		pointsCorrectionsVectors[penetrationIndexForVertex + (point_order_in_stride << 1) + 1] = startColor;
	}
	//pointsMarked[gid] = 0;
}



// intersect ray against plane 
// from https://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-plane-and-ray-disk-intersection
float4 IntersectRayPlane(float3 p0, float3 n, float3 l0, float3 l) {
	float4 result = (float4)(0.0f, 0.0f, 0.0f, 0.0f);

	float denom = dot(l, n);
	float t = 0.0f;
	if (denom > EPSILON) {
		float3 p0l0 = p0 - l0;
		t = dot(p0l0, n) / denom;
	}

	result.xyz = l0 + l*t;
	result.w = t;
	return result;
}


__kernel void ResetControlPoints(__global float* controlPoints) {
	cd_index_type gid = get_global_id(0);
	cd_physical_scalar val = 0.0f;
	controlPoints[gid] = val;
}
__kernel void CreateControlPoints(
	__global float* controlPoints,
	__global float*	randomNumbers,
	Params params)
{
	cd_index_type gid = get_global_id(0);
	cd_index_type gsize = get_global_size(0);
	cd_index_type rows = params.ControlRows;
	cd_index_type cols = params.ControlCols;
	cd_index_type col = gid % cols;
	cd_index_type row = gid / cols;
	cd_index_type currentStep = params.currentStep;
	cd_index_type rowRandomIndex = ((row+ currentStep)%rows) + cols;
	cd_index_type colRandomIndex = (col + currentStep) % cols;
	float val = controlPoints[gid];
	float fcol = convert_float(col);
	float frow = convert_float(row);
	float randomizer = params.Random; 
	float amp = params.Random;
	float random_of_row = randomNumbers[rowRandomIndex];
	float random_of_col = randomNumbers[colRandomIndex];
	cd_physical_scalar WindForceModifier = params.WindForceModifier;
	float vadd = 0.05f*(random_of_row + random_of_col);
	val = val + vadd;
	if (fabs(val) > 1.5f) {
		val = val - 2*vadd;
	}
	controlPoints[gid] = val;
}
// calculating appropriate wind vector from textured coordinates normalized to 1
float3 calculateWindVector(float2 uv, float4 boundaries, __global float* controlPoints) {
	float frows = boundaries.x;
	float fcols = boundaries.y;
	float3 windVector = (float3)(0.0f, 0.0f, 0.0f);
	cd_index_type rows = convert_uint(frows);
	cd_index_type cols = convert_uint(fcols);
	float frow = frows*uv.x;
	float fcol = fcols*uv.y;
	cd_index_type row = convert_uint(frow)+ WIND_FORCE_CONTROL_POINTS_OFFSET_SINGLE_BOUND;
	cd_index_type col = convert_uint(fcol)+ WIND_FORCE_CONTROL_POINTS_OFFSET_SINGLE_BOUND;
	return windVector;
}


//
// update particles flat mode, test changed physics
//
__kernel void update_particles_nvidia_post_update(__global float4* positions,
	__global float4* particlePreviousPosition,
	__global float4* forcesVectors,
	__global float4* pointsFeatures,
	__global float4* pNormalsMovements,
	__global float4* pointsPentrationsVectors,
	__global float4* pointsCorrectionsVectors,
	__global int             *pointsMarked,
	__global float*	randomNumbers,
	__global float*	controlPoints,
	__global cd_physical_scalar* particleInvArea,
	__global cd_index_type* particleForcesIndices,
	__global cd_index_type* particleForcesPerParticle,
	__global cd_index_type* aAabbfeatures,
	__global cd_index_type* pAvatarTrianglesMarked,
	__global cd_index_type* aAabbLeafsTrianglesIndices,
	__global cd_vec4* aAabbBoxes,
	__global cd_vec4* generalPurposeParticleBuffer7, // for tests
	__global cd_vec4* generalPurposeParticleBuffer8, // for tests
	__global cd_vec4* generalPurposeParticleBuffer8p2, // for tests
	__global cd_vec4* generalPurposeParticleBuffer8p3, // for tests
	__global cd_vec4* generalPurposeParticleBuffer9, // for tests
	__global cd_vec4* pAvatarTriangles,
	__global cd_vec4* pAvatarTrianglesNormals,
	__global cd_physical_scalar* particleForcesCoefficents,
	__global cd_bool* particleFixed,
	Params params)
{
	cd_index_type gid = get_global_id(0);
	int markedPoint = pointsMarked[gid];
	if (particleFixed[gid] == 1) {
		return;
	}
	const cd_physical_scalar dtsquare = params.dtsquare;
	const cd_physical_scalar dt = params.dt;
	const cd_physical_scalar invdt = params.invdt;
	float4 position = positions[gid];
	float4 pposition = particlePreviousPosition[gid];
	float4 WindForce = params.WindForceVec;
	float WindForceAmp = params.WindForceAmplitude;
	WindForce = WindForce;
	cd_physical_scalar WindForceModifier = params.WindForceModifier;
	float4 GravityAcc = params.GravityAccVec;
	float3 gravity = GravityAcc.xyz;
	float4 MouseForce = params.vecMouseForce;
	float3 velocity = (position.xyz - pposition.xyz) * invdt;
	if (position.y < EPSILON*-1.0f && params.FloorActive == 1) {
		velocity.y = 0.0f;
	}
	float3 positionCorrection = (float3)(0.0f, 0.0f, 0.0f);
	float4 pointsFeature = (float4)(0.0f, 0.0f, 0.0f, 0.0f); 
	float4 acceleration4 = (float4)(0.0f, 0.0f, 0.0f, 0.0f);
	float4 testNormalVector = (float4)(0.0f, 0.0f, 0.0f, 0.0f);
	float4 testNormalVectorNeutralizer = generalPurposeParticleBuffer8p2[gid<<1];
	if (params.PersistantTestingFeatures != 0) {
		pointsFeature = pointsFeatures[gid]; //(float4)(0.0f, 0.0f, 0.0f, 0.0f); // 
		testNormalVector = generalPurposeParticleBuffer8p2[1+(gid<<1)];
	}
	float3 frictionForce = (float3)(0.0f, 0.0f, 0.0f);
	float3 ForceSum = (float3)(0.0f, 0.0f, 0.0f);
	float4 normalMovement = (float4)(0.0f, 0.0f, 0.0f, 0.0f);
	float4 ZeroedNormal = (float4)(0.0f, 0.0f, 0.0f, 0.0f);
	float3 normalx = (float3)(0.0f, 0.0f, 0.0f);

	// this variables are designated for debugging penetration vector to test correct positioning of
	float4 pentrationColorSource = (float4)(1.0f, 1.0f, 1.0f, 0.0f);
	float4 pentrationColorTarget = (float4)(1.0f, 0.0f, 0.0f, 0.0f);
	float4 penetrationSource = position;
	float4 penetrationTarget = position;
	float4 correctionColorSource = pentrationColorTarget;
	float4 correctionColorTarget = (float4)(0.0f, 0.0f, 1.0f, 0.0f);
	float4 correctionSource = position;
	float4 correctionTarget = position;
	// each vertex has position,position color,penetration target and penetration target color, so for indices per vertex, shr by 2 bits
	cd_index_type penetrationIndexForVertex = gid << 2;

	float4 lastTestVector = generalPurposeParticleBuffer7[gid];
	cd_physical_scalar invArea = particleInvArea[gid];
	cd_index_type start_index_coefficent = MAX_FORCES_PER_PARTICLE*gid;
	cd_index_type end_index_coefficent = start_index_coefficent + particleForcesPerParticle[gid];
	float pInvMass = params.invParticleMass* invArea;
	//if (params.BendActive != 0.0f && params.StretchActive != 0.0f) {
		for (cd_index_type force_id = start_index_coefficent; force_id < end_index_coefficent; force_id++) {
			cd_index_type force_index = particleForcesIndices[force_id];
			cd_physical_scalar coeff = particleForcesCoefficents[force_id];
			float4 forceVector = forcesVectors[force_index];
			/*if (gid == 1975) {
				printf("gid(%u,fi=%u)=(x:%6.3f,y:%6.3f,z:%6.3f)\n", gid, force_index, forceVector.x, forceVector.y, forceVector.z);
			}*/
			if (length(forceVector.xyz) > 10 * EPSILON) {
				ForceSum = ForceSum + coeff * forceVector.xyz;
			}
		}
	//}
	float3 airDragForce = velocity*-1.0f* params.AirDragFroceCoefficient;

	float3 LocalWindForceFactor = (float3)(0.0f, 0.0f, 0.0f);
	float  LocalWindForce = 0.0f;
	cd_index_type currentStep = params.currentStep;
	float currentStepf = convert_float(currentStep);
	cd_index_type cols = params.ControlCols;
	cd_index_type rows = params.ControlRows;
	const float spatialFilerFactor = params.WindSpatialDivisionFactor;
	const uint spatialDistanceFactor = convert_uint(1.0f / spatialFilerFactor);
	cd_index_type ucol = spatialDistanceFactor*convert_uint(fabs(spatialFilerFactor*position.x));
	cd_index_type urow = spatialDistanceFactor*convert_uint(fabs(spatialFilerFactor*position.y));
	ucol = (ucol + currentStep) % cols;
	urow = (urow - currentStep) % rows;
	cd_physical_scalar ColAngle = 0.0f;
	cd_physical_scalar RowAngle = 0.0f;
	float CPWindPower = 0.0f;
	
	for (int i1 = 0; i1 < 5; i1++) {
		for (int i2 = 0; i2 < 5; i2++) {
			cd_index_type control_point_index = (urow + i1)*cols + ucol + i2;
			CPWindPower = controlPoints[control_point_index];
			
			ColAngle = randomNumbers[ucol+rows];
			RowAngle = randomNumbers[urow];
			ColAngle = ColAngle;
			RowAngle = RowAngle;
			LocalWindForceFactor.x = LocalWindForceFactor.x + CPWindPower*sin(0.4f*currentStepf+ColAngle);
			LocalWindForceFactor.y = LocalWindForceFactor.y + CPWindPower*sin(0.4f*currentStepf+RowAngle);
			LocalWindForce = LocalWindForce + CPWindPower*sin(RowAngle*currentStepf)+CPWindPower*cos(ColAngle*currentStepf);
		}
	}
	if (length(LocalWindForceFactor) > EPSILON) {
		LocalWindForceFactor = normalize(LocalWindForceFactor);
	}
	else {
		LocalWindForceFactor.x = 0.707f;
		LocalWindForceFactor.y = 0.707f;
	}
	LocalWindForceFactor = LocalWindForceFactor*(0.04f*CPWindPower/WindForceModifier);
	/*if (markedPoint == 1) {
		printf("gid(%u),LocalWindForceFactor:(%6.3e,%6.3e)\n", gid, LocalWindForceFactor.x, LocalWindForceFactor.y);
		
	}*/
	// note for total force on the particle we need to include the gravity force too...
	float3 netForce = ForceSum.xyz;// +airDragForce;
	netForce = netForce + WindForceAmp*WindForce.xyz;
	if (fabs(LocalWindForce) > EPSILON) {
		netForce = netForce + LocalWindForce* WindForceModifier*WindForce.xyz;
	}
	//if (lastTestVector.w < 0.0f) {
		// add wind only if no collission detected
		//netForce = netForce + LocalWindForceFactor;
	//}
	//netForce = netForce + frictionForce;

	float3 acceleration = netForce * pInvMass;
	//// reduce gravity part from last hit
	//if (testNormalVectorNeutralizer.y > 0) {
	//	gravity.y = (1 - testNormalVectorNeutralizer.y)*gravity.y;
	//}
	if (params.GravityActive == 1 && pInvMass < INVEPSILON ) {
		acceleration = acceleration + gravity;
	}

	/*if (markedPoint == 1) {
		printf("gid(%u)=(x:%6.3e,y:%6.3e,z:%6.3e)\n", gid, acceleration.x, acceleration.y, acceleration.z);
	}*/
	acceleration4.xyz = acceleration;
	generalPurposeParticleBuffer8p3[gid*3+1] = acceleration4;
	generalPurposeParticleBuffer9[gid] = ZeroedNormal;
	generalPurposeParticleBuffer8p3[gid * 3] = pointsFeature;
	// reduce normal of previous contact from the triangle
	if (lastTestVector.w >= 0.0f && lastTestVector.w <= 1.0f) {
		acceleration4.xyz = dot(acceleration, testNormalVectorNeutralizer.xyz)*testNormalVectorNeutralizer.xyz;
		acceleration4.w = dot(acceleration, testNormalVectorNeutralizer.xyz);
		generalPurposeParticleBuffer9[gid] = testNormalVectorNeutralizer;
		generalPurposeParticleBuffer8p3[gid * 3] = acceleration4;
		//acceleration = acceleration - dot(acceleration, testNormalVectorNeutralizer.xyz)*testNormalVectorNeutralizer.xyz;
	}
	barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);
	testNormalVectorNeutralizer = (float4)(0.0f, 0.0f, 0.0f, 0.0f);
	acceleration4.xyz = acceleration;
	acceleration4.w = 0.0f;
	generalPurposeParticleBuffer8p3[(gid * 3)+2] = acceleration4;
	float4 newPosition = (float4)(0.0f, 0.0f, 0.0f, 1.0f);
	float4 testVector = (float4)(0.0f, 0.0f, 0.0f, 0.0f);
	
	//
	//position.xyz = position.xyz + positionCorrection;
	cd_physical_scalar fpposFactor = params.fPPosFactor;
	fpposFactor = 1.0f - fpposFactor;
	newPosition.xyz = position.xyz + fpposFactor*(position.xyz -pposition.xyz) + acceleration*dtsquare;
	//if (newPosition.y < -EPSILON) {
	//	newPosition.y = EPSILON;
	//}
	float3 movement = newPosition.xyz - position.xyz;
	
	// searching for intersection post movement
	uint4 aabb_containers_ids = findDeepestContainingAabbfindBoth(aAabbfeatures, aAabbBoxes, newPosition.xyz,position.xyz);
	cd_index_type aabb_container_id = aabb_containers_ids.x;//findDeepestContainingAabb(aAabbfeatures, aAabbBoxes, newPosition.xyz);
	

	cd_index_type feature_id = aabb_container_id << 2;
	cd_index_type feature_id_start = aabb_containers_ids.y << 2;
	cd_index_type triangles_for_aabb = aAabbfeatures[feature_id + 2];
	bool found_intersection = false;
	if (feature_id > 0 && params.CollisionActive == 1) {

		cd_index_type box_feature_id = aabb_container_id << 1; // there are min and max points for every box
		cd_vec4 boxMin = aAabbBoxes[box_feature_id];
		cd_vec4 boxMax = aAabbBoxes[box_feature_id + 1];
		cd_vec4 boxSize = fabs(boxMax - boxMin);

		// for each direction of movement vector find its relation to leaf box
		float3 dimensionsRelations = movement / boxSize.xyz;
		cd_physical_scalar maxRelationf = 2*ceil(fmax(dimensionsRelations.x, fmax(dimensionsRelations.y, dimensionsRelations.z)));
		cd_index_type maxRelation = convert_uint(maxRelationf);
		float3 jumpVector = movement / maxRelationf; // we will check points a lon the movement vector by this value
		
		// we will scan this path if either we found triangles ath the end of the path 
		// or there is more than one point along the path to check
		if (triangles_for_aabb > 0 || maxRelation > 1) {
			float3 testedPoint = position.xyz;
			for (cd_index_type testedIntervalIndex = 0; testedIntervalIndex <= maxRelation&&!found_intersection; testedIntervalIndex++) {
				
				// we test to find relevant leaf container, starts from parent box
				aabb_container_id = findDeepestContainingAabb(aAabbfeatures, aAabbBoxes, testedPoint, aabb_containers_ids.y);
				// now we find for tested interval its bounding box index as feature_id
				feature_id = aabb_container_id << 2;
				triangles_for_aabb = aAabbfeatures[feature_id + 2];
				
				// we will check only points taht are in boxes with triangles
				
				if (triangles_for_aabb > 0 && feature_id > 0) {
					cd_index_type leaf_index = aAabbfeatures[feature_id + 3];
					cd_index_type trianglesIndicesOffset = AABB_MAX_TRIANGLES_DEF*leaf_index;
					cd_index_type i = 0;
					
					while (i<triangles_for_aabb && !found_intersection) {
						cd_index_type triangleFeatureNormalIndex = aAabbLeafsTrianglesIndices[trianglesIndicesOffset + i];
						cd_index_type triangleFeatureIndex = triangleFeatureNormalIndex * 3;
						cd_vec4 v0 = pAvatarTriangles[triangleFeatureIndex];
						cd_vec4 v1 = pAvatarTriangles[triangleFeatureIndex + 1];
						cd_vec4 v2 = pAvatarTriangles[triangleFeatureIndex + 2];
						cd_vec4 nrml = pAvatarTrianglesNormals[triangleFeatureNormalIndex];
						testVector = IntersectTriangleLocal(v0, v1, v2, newPosition, position, true);
						//if (markedPoint == 1 && gid == 1975) {
						//	printf("gid(%u,%d),returnVector,0:(%6.3f,%6.3f,%6.3f,%6.3f),1:(%6.3f,%6.3f,%6.3f)\n", gid,i, testVector.x, testVector.y, testVector.z, testVector.w, testedPoint.x, testedPoint.y, testedPoint.z);
						//	//printf("gid(%u),movement_on_normal,0:(%6.3f,%6.3f,%6.3f)\n", gid, movement_on_normal.x, movement_on_normal.y, movement_on_normal.z);
						//}
						if (testVector.w>0.0f && testVector.w<=1.0f) {
							//if(didParticleCrossTriangleBetweenSteps(triangles[triangleIndex], particles[gid].pos, particles[gid].ppos)){
							//pAvatarTrianglesMarked[triangleFeatureNormalIndex] = 1;
							normalx = normalize(nrml.xyz);
							pointsFeature.x = 1.0f;
							testNormalVector.xyz = normalx;
							testNormalVectorNeutralizer.xyz = normalx;
							//
							float3 triangleAvg = (v0.xyz + v1.xyz + v2.xyz)*0.333f;
							float4 normalIntersection = IntersectRayPlane(triangleAvg.xyz, normalx, newPosition.xyz, normalx);
							float4 intersectionData = testVector;// IntersectRayPlane(v0.xyz, normalx, position.xyz, newPosition.xyz - position.xyz);
							//float4 normalIntersection = IntersectRayTriangle(v0, v1, v2, newPosition.xyz, normalx); // test intersection
							float3 nixyz = normalIntersection.xyz;
							float3 movement_on_normal = (float3)(0.0f, 0.0f, 0.0f);
							//if (normalIntersection.w > 0.0f) {
							float fixSizedDiff = params.NormalDistanceFromCollision;
							float3 returnVector = normalx;// normalize(position.xyz - newPosition.xyz);
							float3 cross_section = testVector.xyz - newPosition.xyz;
							float3 position3 = position.xyz;
							float3 newPosition3 = newPosition.xyz;
							float3 dposition3 = position3 - newPosition3;
							float blength = length(dposition3);
							float crossSectionLength = length(cross_section);
							
							float3 backmovement = dposition3;
							/*backmovement.x = backmovement.x / blength;
							backmovement.y = backmovement.y / blength;
							backmovement.z = backmovement.z / blength;*/
							float3 normalsReturn = nixyz - newPosition.xyz;
							float normalReturnsSize = length(normalsReturn);
							normalsReturn = normalize(normalsReturn);
							movement_on_normal = testVector.w*backmovement;
							/*if (pointsMarked[gid] == 1) {
								printf("gid(%u),blength=%6.3e,crossSectionLength=%6.3e,cross_section(%6.3e,%6.3e,%6.3e),crossSectionLength*backmovement(%6.3e,%6.3e,%6.3e)backmovement(%6.3e,%6.3e,%6.3e)\n", gid, blength, crossSectionLength, cross_section.x, cross_section.y, cross_section.z, movement_on_normal.x, movement_on_normal.y, movement_on_normal.z, backmovement.x, backmovement.y, backmovement.z);
							}*/
							
							movement_on_normal = movement_on_normal + fixSizedDiff*returnVector;
							
							//}
							//movement_on_normal = (testVector.xyz - newPosition.xyz) *(1.0f + params.PositionCorrectionCoefficient);
							//movement_on_normal.y = 1.01f*(normalIntersection.y - newPosition.y);
							float3 movement_on_triangle = movement*(1 - intersectionData.w) - movement_on_normal;
							float3 frictionAccelerationd2 = dot(netForce, normalx)*params.AvatarFritionCoefficient*pInvMass*normalize(movement_on_triangle);
							normalMovement.xyz = movement_on_normal;
							normalMovement.w = 1.0f;// convert_float(triangleFeatureNormalIndex);
							//normalMovement = testVector;
							float3 fixVector = frictionAccelerationd2*dtsquare*(1 - intersectionData.w)*(1 - intersectionData.w); // previous movement into the triangle that need to be fixed

							positionCorrection = movement_on_normal+fixVector;// *clamp(fabs(normalize(movement_on_normal).y), 0.001f, 1.0f);//direction*0.3f;
							
							/*if (markedPoint == 1) {
								printf("gid(%u,triangleFeatureNormalIndex=%d),normalx,0:(%6.3f,%6.3f,%6.3f)\n", gid, triangleFeatureNormalIndex, normalx.x, normalx.y, normalx.z);
								printf("gid(%u),positionCorrection,0:(%6.3f,%6.3f,%6.3f),testVector,0:(%6.3f,%6.3f,%6.3f),newPosition,0:(%6.3f,%6.3f,%6.3f),position,0:(%6.3f,%6.3f,%6.3f)\n", gid, positionCorrection.x, positionCorrection.y, positionCorrection.z, testVector.x, testVector.y, testVector.z, newPosition.x, newPosition.y, newPosition.z, position.x, position.y, position.z);
							}*/
							penetrationTarget = newPosition;
							correctionSource = newPosition;
							correctionTarget = newPosition;
							
							// updatin intersection color by positiveness of normal intersection with triangle should be approach 1, yellow is good, red is very bad
							pentrationColorSource.w = 1.0f;
							// the value for position correction length should match smaller than new position - position, if the color is teal its very bad
							float mDistance = clamp(length(newPosition.xyz - position.xyz), 0.0001f, 1000.0f);
							pentrationColorSource.x = clamp(1.0f - 0.5f*length(positionCorrection) / mDistance, 0.0f, 1.0f);
							pentrationColorTarget.w = 1.0f;
							pentrationColorTarget.y = clamp(0.5f + 0.5f*normalIntersection.w, 0.0f, 1.0f);
							correctionColorSource = pentrationColorTarget;
							correctionColorTarget.w = 1.0f;


							
							correctionTarget.xyz = correctionSource.xyz + positionCorrection;
							//newPosition.xyz = normalIntersection.xyz;
							//gravity = (float3)(0.0f, 0.0f, 0.0f);
							//particleFixed[gid] = 1;
							found_intersection = true;
							if (params.LockParticle == 1) {
								particleFixed[gid] = 1;
							}
							
						}
						
						i++;
					}
				}

				// find next position to test on path, 
				testedPoint = testedPoint + jumpVector;
				
			}
		}
	}
	barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);
	newPosition.xyz = newPosition.xyz+ positionCorrection;
	//position.xyz = position.xyz;// +positionCorrection;
	if (found_intersection) {
		
		position.xyz = newPosition.xyz;
	}
	newPosition.w = 1.0f;
	position.w = 1.0f;

	particlePreviousPosition[gid] = position;
	positions[gid] = newPosition;
	pointsFeatures[gid] = pointsFeature;

	generalPurposeParticleBuffer7[gid] = testVector;
	float4 tnvector = testNormalVector;
	tnvector.w = 1.0f;
	generalPurposeParticleBuffer8[gid] = tnvector;
	generalPurposeParticleBuffer8p2[1 + (gid << 1)] = testNormalVector;
	generalPurposeParticleBuffer8p2[gid << 1] = testNormalVectorNeutralizer;
	pNormalsMovements[gid] = normalMovement;
	// set the data for penetration vector rendering
	pointsPentrationsVectors[penetrationIndexForVertex] = penetrationSource;
	pointsPentrationsVectors[penetrationIndexForVertex+1] = pentrationColorSource;
	pointsPentrationsVectors[penetrationIndexForVertex+2] = penetrationTarget;
	pointsPentrationsVectors[penetrationIndexForVertex+3] = pentrationColorTarget;

	pointsCorrectionsVectors[penetrationIndexForVertex] = correctionSource;
	pointsCorrectionsVectors[penetrationIndexForVertex + 1] = correctionColorSource;
	pointsCorrectionsVectors[penetrationIndexForVertex + 2] = correctionTarget;
	pointsCorrectionsVectors[penetrationIndexForVertex + 3] = correctionColorTarget;
	barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);
}

__kernel void SetParticlePosition(__global float4             *positions,
	__global float4* particlePreviousPosition,
	Params params)
{

	float4 vecPosition = positions[params.IndexParticle];
	float4 vecPposition = particlePreviousPosition[params.IndexParticle];
	barrier(CLK_GLOBAL_MEM_FENCE);

	particlePreviousPosition[params.IndexParticle] = vecPosition;
	positions[params.IndexParticle] = params.vecMouseForce;

}

float vertexDistanceFromLine(float3 p, float3 p1, float3 p2) {
	float sqlength = length(p1 - p2);
	sqlength = sqlength*sqlength;
	float nom = dot(p - p1, p2 - p1);
	float coeff = nom / sqlength;
	float3 vec = p - p1 - coeff*(p2 - p1);
	return length(vec);
}

__kernel void IntersectParticle(__global float4            *positions,
	__global float             *vDistance,
	__global int             *pointsMarked,
	Params             params) {
	cd_index_type gid = get_global_id(0);
	float4 RayEnd = params.vecEndRay;
	float4 RayStart = params.vecStartRay;
	float4 particle = positions[gid];
	float dist = vertexDistanceFromLine(particle.xyz, RayStart.xyz, RayEnd.xyz);
	vDistance[gid] = dist;
	//if (gid == 1000) printf("gid(%u)=x=%6.3f,y=%6.3f,z=%6.3f,x=%6.3f,y=%6.3f,z=%6.3f,x=%6.3f,y=%6.3f,z=%6.3f,d=%6.3f\n", gid, particle.x, particle.y, particle.z, RayStart.x, RayStart.y, RayStart.z, RayEnd.x, RayEnd.y, RayEnd.z, dist);
	barrier(CLK_GLOBAL_MEM_FENCE | CLK_LOCAL_MEM_FENCE);
}

__kernel void ResetMarkers(__global float4            *positions,
	__global int             *pointsMarked,
	Params             params) {
	cd_index_type gid = get_global_id(0);
	pointsMarked[gid] = 0;
}
__kernel void LocateIntersectedParticle(
	__global cd_physical_scalar             *vDistance,
	__global cd_physical_scalar             *vDistanceTemp,
	__global cd_index_type             *vIndexTemp,
	__global int             *pointsMarked,
	//__local cd_physical_scalar	   *cacheDistance, // for local address divides
	//__local cd_index_type	   *cacheIndices, // for local address divides
	Params             params) {
	cd_index_type gid = get_global_id(0);
	cd_index_type gsize = get_global_size(0);
	cd_index_type bsize = params.BatchSize;
	cd_index_type cindex = bsize + 1u;
	cd_index_type selector = 0;
	float fselector = 0.0f;
	cd_index_type dividers = bsize / gsize;
	if (bsize % gsize > 0u) {
		dividers = dividers + 1;
	}
	cd_index_type alterIndex = bsize + 1u;
	cd_index_type secondIndex;
	cd_index_type iselector;
	cd_physical_scalar dist = -1.0f;
	cd_physical_scalar alternateDistance = -1.0f;
	cd_physical_scalar cdist = -1.0f;

	// stride scan, every thread sum its own data
	cd_index_type i = 0;
	//cacheIndices[gid] = cindex;
	//cacheDistance[gid] = dist;
	barrier(CLK_GLOBAL_MEM_FENCE | CLK_LOCAL_MEM_FENCE);
	while ( i < dividers ) {
		iselector = ((gid+i*gsize) < bsize ? (i*gsize) : ((i-1u)*gsize))+gid;
		cdist = vDistance[iselector];

		selector = (dist < 0.0f || (dist > cdist && cdist > 0.0f)) ? 1u : 0u;
		fselector = convert_float(selector);
		dist = fselector*cdist + (1.0f-fselector)*dist;
		cindex = selector*iselector + (1u-selector)*cindex;
		vDistanceTemp[gid] = dist;
		vIndexTemp[gid] = cindex;
		//

		barrier(CLK_GLOBAL_MEM_FENCE | CLK_LOCAL_MEM_FENCE);
		/*if (gid < 32) {
			printf("i=%u,cindex=%u,ig=%u,dist=%6.3f,cdist=%6.3f\n", i, cindex,i*gsize,dist, cdist);
		}*/
		i++;
	}
	//barrier(CLK_GLOBAL_MEM_FENCE | CLK_LOCAL_MEM_FENCE);
	//cacheIndices[gid] = cindex;
	//cacheDistance[gid] = dist;
	// coordinate all local data
	barrier(CLK_GLOBAL_MEM_FENCE|CLK_LOCAL_MEM_FENCE);
	cd_index_type remainStride = gsize >> 1u;
	dist = vDistanceTemp[gid];
	cindex = vIndexTemp[gid];
	while ( remainStride > 0u) {
		if (gid < remainStride) {
			cindex = vIndexTemp[gid];
			dist = vDistanceTemp[gid];
			secondIndex = gid + remainStride;
			alternateDistance = vDistanceTemp[secondIndex];
			alterIndex = vIndexTemp[secondIndex];
			

			selector = ((dist > alternateDistance || dist < EPSILON) && alternateDistance > EPSILON && alterIndex < bsize) ? 1u : 0u;
			fselector = convert_float(selector);
			// alternate distance is set and smaller then current distance, or current distance not set
			vIndexTemp[gid] = selector*alterIndex + (1u-selector)*cindex;
			vDistanceTemp[gid] = fselector*alternateDistance + (1.0f - fselector)*dist;
		}
		barrier(CLK_GLOBAL_MEM_FENCE | CLK_LOCAL_MEM_FENCE);
		/*if (gid  == 0u) {
			printf("remainStride=%u,alterIndex=%u,alternateDistance=%6.3f,cindex=%u,dist=%6.3f,bsize=%u\n", remainStride, alterIndex, alternateDistance, cindex, dist, bsize);
		}*/
		remainStride = remainStride >> 1u;
	}
	barrier(CLK_GLOBAL_MEM_FENCE | CLK_LOCAL_MEM_FENCE);
	int markerUnit = 0;
	if (gid == 0u) {

		
		//printf("markerUnitIII=%d\n", markerUnit);
	}
	barrier(CLK_GLOBAL_MEM_FENCE | CLK_LOCAL_MEM_FENCE);
}

__kernel void CopyIntersectedParticle(
	__global cd_physical_scalar             *vDistance,
	__global cd_physical_scalar             *vDistanceTemp,
	__global cd_index_type             *vIndexTemp,
	__global int             *pointsMarked,
	//__local cd_physical_scalar	   *cacheDistance, // for local address divides
	//__local cd_index_type	   *cacheIndices, // for local address divides
	Params             params) {
	cd_index_type gid = get_global_id(0);
	cd_index_type cindex = vIndexTemp[gid];
	cd_physical_scalar dist = vDistanceTemp[gid];
	//printf("cindex=%u,dist=%6.3e2f\n", cindex, dist);
	int markerUnit = pointsMarked[cindex];
	barrier(CLK_GLOBAL_MEM_FENCE | CLK_LOCAL_MEM_FENCE);
	//printf("markerUnit=%d\n", markerUnit);
	if (params.MouseMarkerMode == 1) {
		markerUnit = 1;
	}
	else if (params.MouseMarkerMode == 2) {
		markerUnit = 0;
	}
	else if (params.MouseMarkerMode == 3) {
		markerUnit = 1 - markerUnit;
	}
	barrier(CLK_GLOBAL_MEM_FENCE | CLK_LOCAL_MEM_FENCE);
	//printf("markerUnitII=%d\n", markerUnit);
	pointsMarked[cindex] = markerUnit;
}

//
//  IntersectTriangle
// Fast  Minimum Storage Ray Triangle Intersection
// this calculates normals as well
__kernel void GarmentTrianglesNormals(__global float4            *positions,
	__global cd_index_type         *pTriangles,
	__global float             *pDistance,
	__global cd_vec4             *normTriangles,
	Params             params)
{
	cd_index_type gid = get_global_id(0);
	cd_index_type gsize = get_global_size(0);
	pDistance[gid] = -1.0f;
	barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);
	float fT = 0.0f;
	float fU = 0.0f;
	float fV = 0.0f;

	float4 RayEnd = params.vecEndRay;
	float4 RayStart = params.vecStartRay;

	cd_index_type Index1 = pTriangles[3 * gid];
	cd_index_type Index2 = pTriangles[3 * gid + 1];
	cd_index_type Index3 = pTriangles[3 * gid + 2];

	float3 Vertex1 = positions[Index1].xyz;
	float3 Vertex2 = positions[Index2].xyz;
	float3 Vertex3 = positions[Index3].xyz;

	// Find vectors for two edges sharing vert1
	float3 Edge1 = Vertex2 - Vertex1;
	float3 Edge2 = Vertex3 - Vertex1;
	float3 triNormal = cross(Edge1, Edge2);
	float4 triNormal4 = (float4)(0.0f, 0.0f, 0.0f, 0.0f);
	triNormal4.xyz = triNormal.xyz;
	normTriangles[gid] = triNormal4;

}

__kernel void GarmentParticleNormals(__global float4            *positions,
	__global cd_index_type         *pTriangles,
	__global float             *pDistance,
	__global cd_vec4             *normTriangles,
	__global cd_index_type	*particlesNumberOfTriangles,
	__global cd_index_type	*particlesTrianglesIndices,
	__global cd_vec4		*garmentNormals,
	__global cd_vec4	*pNormalsParticles,
	Params             params)
{
	cd_index_type gid = get_global_id(0);
	cd_index_type gsize = get_global_size(0);
	cd_index_type particleNumberOfTriangles = particlesNumberOfTriangles[gid];
	cd_index_type trianglesIndicesStart = gid*MAX_TRIANGLES_PER_PARTICLE;
	cd_index_type trianglesIndices = trianglesIndicesStart;
	float4 normTriangle4 = (float4)(0.0f, 0.0f, 0.0f, 0.0f);
	float3 nrmTriangle = normTriangle4.xyz;

	float4 normParticle4 = (float4)(0.0f, 0.0f, 0.0f, 0.0f);
	float3 nrmParticle = normParticle4.xyz;
	cd_index_type triangle_actual_indices = 0;
	
	for (cd_index_type tcounter = 0; tcounter < particleNumberOfTriangles; tcounter++) {
		trianglesIndices = trianglesIndicesStart + tcounter;
		
		triangle_actual_indices = particlesTrianglesIndices[trianglesIndices];
		/*if (triangle_actual_indices >= 21140) {
			printf("gid(%u)->triangle_actual_indices=%d\n", gid, triangle_actual_indices);
		}*/
		normTriangle4 = normTriangles[triangle_actual_indices];
		nrmTriangle = normTriangle4.xyz;
		nrmParticle = nrmParticle + nrmTriangle;
	}
	nrmParticle = normalize(nrmParticle);
	normParticle4.xyz = nrmParticle;
	normParticle4.w = 0.0f;
	garmentNormals[gid] = normParticle4;
	pNormalsParticles[gid] = normParticle4;
}

//
//  IntersectTriangle
// Fast  Minimum Storage Ray Triangle Intersection
// this calculates normals as well
__kernel void IntersectTriangle(__global float4            *positions,
	__global cd_index_type         *pTriangles,
	__global float             *pDistance,
	__global cd_vec4             *normTriangles,
	Params             params)
{

	size_t gid = get_global_id(0);
	pDistance[gid] = -1.0f;
	barrier(CLK_LOCAL_MEM_FENCE|CLK_GLOBAL_MEM_FENCE);
	float fT = 0.0f;
	float fU = 0.0f;
	float fV = 0.0f;

	float4 RayEnd =  params.vecEndRay;
	float4 RayStart = params.vecStartRay;

	cd_index_type Index1 = pTriangles[3*gid];
	cd_index_type Index2 = pTriangles[3*gid+1];
	cd_index_type Index3 = pTriangles[3*gid+2];

	float3 Vertex1 =  positions[Index1].xyz;
	float3 Vertex2 =  positions[Index2].xyz;
	float3 Vertex3 =  positions[Index3].xyz;

	// Find vectors for two edges sharing vert1
	float3 Edge1 = Vertex2 - Vertex1;
	float3 Edge2 = Vertex3 - Vertex1;
	float3 triNormal = cross(Edge1, Edge2);
	float4 triNormal4 = (float4)(0.0f, 0.0f, 0.0f,1.0f);
	triNormal4.xyz = triNormal.xyz;
	normTriangles[gid] = triNormal4;

	float4 RayDirection4 = RayEnd - RayStart;
	float3 RayDirection = RayDirection4.xyz;
	float3 RayDirNormalize = normalize(RayDirection.xyz);

	// Begin calculating determinant - alos used to calculate U parameter
	
	float3 VecCross = cross(RayDirNormalize, Edge2);

	// If determinant is near zero, ray lies in plane of triangle
	//float fDet = (float)dot((float3)Edge1, float3(VecCrossNarrow));
	float fDet = dot(Edge1, VecCross);

	if ((-EPSILON >= fDet) || (EPSILON <= fDet)) {

		float InvDet = 1.0f / fDet;

		// Calculate distance from vert1 to ray origin
		float3 DisVert1RayOrigin = RayStart.xyz - Vertex1;

		// Calculate U parameter and test bounds
		fU = dot(DisVert1RayOrigin, VecCross) * InvDet;

		if ((0.0f <= fU) && (1.0f >= fU)) {

			// Prepare to test V parameter
			float3 vecCrossVParam = (float3)(0.0f, 0.0f, 0.0f);
			vecCrossVParam = cross(DisVert1RayOrigin, Edge1);
			//Calculate V parameter and test bounds
			fV = dot(RayDirNormalize, vecCrossVParam) * InvDet;

			if ((0.0f <= fV) && (1.0f >= fU + fV)) {
				// Calculate t, ray intersects triangle
				fT = dot(Edge2, vecCrossVParam) *InvDet;
				pDistance[gid] = fT;
			}
		}
	}
	barrier(CLK_GLOBAL_MEM_FENCE);

}

//
//  UpdateTriangleByDistance
//
__kernel void UpdateTriangleByDistanceStride(__global float4             *positions,
	__global float             *pDistance,
	__global cd_index_type         *pTrianglesInputTemp,
	__global float             *pDistanceInputTemp,
	__global cd_index_type         *pTrianglesOutputTemp,
	__global float             *pDistanceOutputTemp,
	__local float	   *cacheDistance, // for local address divides
	__local cd_index_type	   *cacheIndices, // for local address divides
	Params              params)
{
	cd_index_type strideNumber = params.strideNumber;
	cd_index_type totalTriangles = params.CountTriangles;
	cd_index_type gid = get_global_id(0);
	
	cd_index_type strideSize = (cd_index_type)get_local_size(0);
	cd_index_type strideId = (cd_index_type)(gid / strideSize);
	cd_index_type cacheId = (cd_index_type)get_local_id(0);
	if (strideNumber == 0) { 
		cacheIndices[cacheId] =gid;
		if (gid<totalTriangles) cacheDistance[cacheId] = (float)pDistance[gid];
		else cacheDistance[cacheId] = (float)-1.0f;
	}
	else { 
		cacheIndices[cacheId] = pTrianglesInputTemp[gid];
		cacheDistance[cacheId] = (float)pDistanceInputTemp[gid];
	}
	barrier(CLK_LOCAL_MEM_FENCE| CLK_GLOBAL_MEM_FENCE);
	float l2size = (float)strideSize;
	cd_index_type local_index = 0;
	float dist = -1.0f;
	cd_index_type second_local_index = 0;
	float second_dist = -1.0f;
	l2size = floor(log2(l2size));
	cd_index_type StrideRun = (cd_index_type)pow(2, l2size);
	cd_index_type secondIndex = 1024;
	if (StrideRun == strideSize) StrideRun >>= 1;
	cd_index_type ThreadsNumber = StrideRun;
	for (ThreadsNumber = StrideRun; ThreadsNumber > 0; ThreadsNumber >>= 1) {
		secondIndex = cacheId + ThreadsNumber;
		if (cacheId < ThreadsNumber && secondIndex < strideSize) {
			local_index = cacheIndices[cacheId];
			dist = cacheDistance[cacheId];
			second_local_index = cacheIndices[secondIndex];
			second_dist = cacheDistance[secondIndex];
			cd_index_type selector = (dist < 0.0f || (second_dist > 0.0f && second_dist < dist))?1:0;
			float dist_selector = (float)selector;
			// its automatic can only be the other one
			cacheDistance[cacheId] = dist_selector*second_dist + ((1- dist_selector)*dist);
			cacheIndices[cacheId] = selector*second_local_index + ((1 - selector)*local_index);
			
		}
		
		barrier(CLK_LOCAL_MEM_FENCE);
	}
	//barrier(CLK_GLOBAL_MEM_FENCE);
	if (cacheId == 0) {
		pDistanceOutputTemp[strideId] = cacheDistance[0];
		pTrianglesOutputTemp[strideId] = cacheIndices[0];
		
	}
	barrier(CLK_GLOBAL_MEM_FENCE| CLK_LOCAL_MEM_FENCE);
}

//
//  UpdateTriangleByDistance
//
__kernel void UpdateTriangleByDistance(__global float4             *positions,
	__global float4* particlePreviousPosition,
	__global cd_index_type         *pTriangles,
	__global float             *pDistance,
	__global cd_index_type         *pTrianglesOutputTemp,
	__global float             *pDistanceOutputTemp,
	__global int             *pointsMarked,
	__global cd_long			*pIndexVertex,
	__global cd_bool			*particleFixed,
	Params              params)
{

	int mouseEvent = params.MouseEvent;
	int mouseMarkerMode = params.MouseMarkerMode;
	float4 RayEnd4 = params.vecEndRay;
	float4 RayStart4 = params.vecStartRay;
	cd_index_type lIndexTriangle = pTrianglesOutputTemp[0];
	//printf("lIndexTriangle:%u\n", lIndexTriangle);
	float4 position = (float4)(0.0f, 0.0f, 0.0f, 1.0f);
	float4 pposition = (float4)(0.0f, 0.0f, 0.0f, 1.0f);
	float4 fIntersectionPoint4 = (float4)(0.0f, 0.0f, 0.0f, 1.0f);
	long IndexVertex = -1;
	long pIndexVertex1 = -1;
	float fDistanceClosest = pDistanceOutputTemp[0];
	//printf("lIndexTriangle:%u,fDistanceClosest:%6.3f\n", lIndexTriangle, fDistanceClosest);
	if (0.0f <= fDistanceClosest)
	{
		
		float3 RayEnd = RayEnd4.xyz;
		float3 RayStart = RayStart4.xyz;

		float3 RayDirNormalize = normalize(RayEnd - RayStart);

		float3 fIntersectionPoint = RayStart + (RayDirNormalize * fDistanceClosest);
		fIntersectionPoint4.xyz = fIntersectionPoint;
		//printf("pre Index vertex read data\n");
		IndexVertex = pIndexVertex[0];
		//printf("IndexVertex:%lld\n", IndexVertex);
		pIndexVertex1 = pIndexVertex[IndexVertex];
		//printf("pIndexVertex1:%lld\n", pIndexVertex1);
		if (mouseEvent == 100 || 0 > pIndexVertex1)
		{
			cd_index_type IndexOne = pTriangles[3 * lIndexTriangle];
			cd_index_type IndexTwo = pTriangles[3 * lIndexTriangle + 1];
			cd_index_type IndexThree = pTriangles[3 * lIndexTriangle + 2];
			//printf("i1:%u,i2:%u,i3:%u\n", IndexOne, IndexTwo, IndexThree);
			float4 VertexOne4 = positions[IndexOne];
			float4 VertexTwo4 = positions[IndexTwo];
			float4 VertexThree4 = positions[IndexThree];

			float3 VertexOne = VertexOne4.xyz;
			float3 VertexTwo = VertexTwo4.xyz;
			float3 VertexThree = VertexThree4.xyz;

			float fDisOne = distance(VertexOne, fIntersectionPoint);
			float fDisTwo = distance(VertexTwo, fIntersectionPoint);
			float fDisThree = distance(VertexThree, fIntersectionPoint);

			pIndexVertex1 = IndexOne;
			float fShortDis = fDisOne;

			if (fDisTwo < fShortDis)
			{
				fShortDis = fDisTwo;
				pIndexVertex1 = IndexTwo;
			}
			if (fDisThree < fShortDis)
			{
				pIndexVertex1 = IndexThree;
			}
		}
		if (mouseEvent == 100) {
			int markerUnit = pointsMarked[pIndexVertex1];
			barrier(CLK_GLOBAL_MEM_FENCE | CLK_LOCAL_MEM_FENCE);
			//printf("markerUnit=%d\n", markerUnit);
			if (mouseMarkerMode == 1) {
				markerUnit = 1;
			}
			else if (mouseMarkerMode == 2) {
				markerUnit = 0;
			}
			else if (mouseMarkerMode == 3) {
				markerUnit = 1 - markerUnit;
			}
			barrier(CLK_GLOBAL_MEM_FENCE | CLK_LOCAL_MEM_FENCE);
			//printf("markerUnitII=%d\n", markerUnit);
			pointsMarked[pIndexVertex1] = markerUnit;
		} else if (pIndexVertex1 >= 0) {
			
			position = positions[pIndexVertex1];
			position.w = 1.0f;
			pposition = particlePreviousPosition[pIndexVertex1];
			pposition.w = 1.0f;
			pposition = position;
			//printf("mouseEvent:%d,pIndexVertex1:%lld\n", mouseEvent, pIndexVertex1);
			
			particlePreviousPosition[pIndexVertex1] = pposition;

			barrier(CLK_GLOBAL_MEM_FENCE); // verify memory copy properly
			positions[pIndexVertex1] = fIntersectionPoint4;
			barrier(CLK_GLOBAL_MEM_FENCE); // verify memory copy properly

			particleFixed[pIndexVertex1] = 1;

			barrier(CLK_GLOBAL_MEM_FENCE); // verify memory copy properly

			pIndexVertex[IndexVertex] = pIndexVertex1;
			
		}
	}

}


//
//  Mouse Event Response
//
__kernel void MouseEvent(__global float4             *positions,
	__global cd_bool			*particleFixed,
	__global cd_long               *pIndexVertex,
	Params              params)
{
	size_t lIndex = get_global_id(0);
	if ('z' == params.MouseEvent) //(LZ_DOWN == params.MouseEvent)
	{
// un handle all particles
		long iVertex = pIndexVertex[lIndex];
		if (iVertex >= 0) {
			particleFixed[iVertex] = 0;
			pIndexVertex[lIndex] = -1;
			if (lIndex == 0) {
				pIndexVertex[0] = 1;
			}
		}

	}
	else if ('x' == params.MouseEvent) //(LX_DOWN == params.MouseEvent)
	{
		// handle next particle
		if (0 < pIndexVertex[0])
		{
			pIndexVertex[0]++;
		}
	}

	barrier(CLK_GLOBAL_MEM_FENCE); // verify memory copy properly

}