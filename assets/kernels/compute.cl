#define COUTURIER_GPU
#include "CpuGpuSharedDataStructures.h"

#define EPSILON 0.000001f
#define CrossProduct(Result, vHead, vTail)  Result.x = vHead.y*vTail.z - vHead.z*vTail.y; \
                                            Result.y = vHead.z*vTail.x - vHead.x*vTail.z; \
                                            Result.z = vHead.x*vTail.y - vHead.y*vTail.x; \



            //
            //  calc_forces
            //
__kernel void calc_forces(__global float4* positions,
                          __global Particle *particles,
                          __global LinearSpringForce *forces,
                          __global CTriangle  *pTriangles,
                          __global float      *pDistance,
                          __global long       *pIndexVertex,
                          Params params)
{

	
    size_t gid = get_global_id(0);
	
    LinearSpringForce force = forces[gid]; // load force
    Particle particle1 = particles[force.particle1];    // load first particle            
	Particle particle2 = particles[force.particle2];    // load second particle
	if (force.particle2 == 0)  printf("gid=%d,fpi=%d, p1=%d, p2=%d,IH=%d,IT=%d\n", gid, force.pIndex, force.particle1, force.particle2, force.IndexHead, force.IndexTail);
    float fRestLength = force.RestLength; // load rest length
	
	

    float4 diff = particle2.pos - particle1.pos; // particles vector difference
    
	diff.w = 0.0f;
	float dx = length(diff) - fRestLength; //  calculate changed length dx
    float4 calculatedForce = normalize(diff)*dx*force.k; // calculate spring force
    calculatedForce.w = 0.0f;

	
    force.Force = calculatedForce;
	//printf("gid=%ld,f.x=%.3f,f.x=%.3f,f.x=%.3f\n",gid, calculatedForce.x, calculatedForce.y, calculatedForce.z);
    particles[force.particle1].Forces[force.IndexHead] = calculatedForce; // update first particle positive force
    particles[force.particle2].Forces[force.IndexTail] = ((-1) * calculatedForce); // update second particle negative force
 }


            //
            //  update_particles
            //
__kernel void update_particles(__global float4* positions,
                               __global Particle *particles,
                               __global LinearSpringForce *forces,
                               __global CTriangle  *pTriangles,
                               __global float      *pDistance,
                               __global long       *pIndexVertex,
                               Params params)
{
    size_t gid = get_global_id(0);
    Particle p = particles[gid];
    
    if(p.fixed)
        return;
    
    float4 position  = particles[gid].pos;
    float4 pposition = particles[gid].ppos;
    
    const float dt = 0.03f;
    
    float4 WindForce    = params.WindForceVec;
    float4 GravityForce = params.GravityForceVec;
    float4 MouseForce   = params.vecMouseForce;

	float4 ForceSum = WindForce +GravityForce; // {0.01f,0.01f ,0.01f ,0.01f };//

    for(int iForce=0; iForce< 12; iForce++)
    {
        ForceSum = ForceSum + p.Forces[iForce];
    }

    
    float4 acc = ForceSum / p.Mass;
    acc = acc - (position - pposition)*2;
    
    particles[gid].ppos = position;
    
    float fPosFactor  = 2.0f;//1.99f; // 2.0 , 1.99
    float fPPosFactor = 1.0f;//0.99f; // 1.0 , 0.99
    float4 newPosition  = position*fPosFactor - pposition*fPPosFactor + acc * dt * dt;
    
    newPosition.w = 1.0f;
    
	float3 ballCenter = (float3)( 0.0f, 0.0f, params.CollisionBallZPos );
    float dist = distance(ballCenter, newPosition.xyz);
    if(dist< 20.0)
    {
        float3 direction = normalize(position.xyz - ballCenter);
        newPosition.xyz += direction*0.5f;
    }
    
    particles[gid].pos = newPosition;
    positions[gid] = newPosition;
}

            //
            //  SetParticlePosition
            //
__kernel void SetParticlePosition(__global float4             *positions,
                                  __global Particle           *particles,
                                  __global LinearSpringForce  *forces,
                                  __global CTriangle          *pTriangles,
                                  __global float              *pDistance,
                                  __global unsigned long      *pIndexVertex,
                                  Params params)
{
	
   // params.IndexParticle;
   // if(0 <= params.IndexParticle)
  //  {
        float4 vecPosition  = particles[params.IndexParticle].pos;
        float4 vecPposition = particles[params.IndexParticle].ppos;
        
        particles[params.IndexParticle].ppos = vecPosition;
        particles[params.IndexParticle].pos  = params.vecMouseForce;
        
        positions[params.IndexParticle] = params.vecMouseForce;
   // }
	
}

            //
            //  IntersectTriangle
            // Fast  Minimum Storage Ray Triangle Intersection
__kernel void IntersectTriangle(__global float4            *positions,
                                __global Particle          *particles,
                                __global LinearSpringForce *forces,
                                __global CTriangle         *pTriangles,
                                __global float             *pDistance,
                                __global long              *pIndexVertex,
                                         Params             params)
{
	
    size_t gid = get_global_id(0);
    
    pDistance[gid] = -1.0f;
    
    float fT = 0.0f;
    float fU = 0.0f;
    float fV = 0.0f;
    
    float3 RayEnd   = {params.vecEndRay.x  , params.vecEndRay.y  , params.vecEndRay.z};
    float3 RayStart = {params.vecStartRay.x, params.vecStartRay.y, params.vecStartRay.z};

    unsigned int Index1 = pTriangles[gid].m_IndexVertices[0];
    unsigned int Index2 = pTriangles[gid].m_IndexVertices[1];
    unsigned int Index3 = pTriangles[gid].m_IndexVertices[2];
    
    float3 Vertex1 = {positions[Index1].x, positions[Index1].y, positions[Index1].z};
    float3 Vertex2 = {positions[Index2].x, positions[Index2].y, positions[Index2].z};
    float3 Vertex3 = {positions[Index3].x, positions[Index3].y, positions[Index3].z};
    
    // Find vectors for two edges sharing vert1
    float3 Edge1 = Vertex2 - Vertex1;
    float3 Edge2 = Vertex3 - Vertex1;
    
    float4 RayDirection     = params.vecEndRay - params.vecStartRay;
    float4 RayDirNormalize  = normalize(RayDirection);
    float3 RayDirNormNarrow = {RayDirNormalize.x, RayDirNormalize.y, RayDirNormalize.z};
    
    // Begin calculating determinant - alos used to calculate U parameter
    float4 Edge2Wide = {Edge2, 0.0f};
    float4 VecCross  = cross(RayDirNormalize,Edge2Wide);
    float3 VecCrossNarrow = {VecCross.xyz};
    
    // If determinant is near zero, ray lies in plane of triangle
    //float fDet = (float)dot((float3)Edge1, float3(VecCrossNarrow));
    float fDet = dot(Edge1, VecCrossNarrow);

#ifdef TEST_CULL // Define TEST_CULL if culling is desired
    if(EPSILON > fDet)
        return;
    // TODO: Implement it...!
    
#else // The non-culling branch
    if((-EPSILON < fDet) && (EPSILON > fDet))
        return;
    
    float InvDet = 1.0f / fDet;
    
    // Calculate distance from vert1 to ray origin
    float3 DisVert1RayOrigin = RayStart - Vertex1;
    
    // Calculate U parameter and test bounds
    fU = dot(DisVert1RayOrigin, VecCrossNarrow) * InvDet;
    
    if((0.0f > fU) || (1.0f < fU))
        return;
    
    // Prepare to test V parameter
    float3 vecCrossVParam = (float3)(0.0f, 0.0f, 0.0f);
    CrossProduct(vecCrossVParam, DisVert1RayOrigin, Edge1);
    
    //Calculate V parameter and test bounds
    fV = dot(RayDirNormNarrow, vecCrossVParam) * InvDet;
    
    if((0.0f > fV) || (1.0f < fU+fV))
        return;
    
    // Calculate t, ray intersects triangle
    fT = dot(Edge2, vecCrossVParam) *InvDet;
    
    pDistance[gid] = fT;
    
#endif

}

            //
            //  UpdateTriangleByDistance
            //
__kernel void UpdateTriangleByDistance(__global float4             *positions,
                                       __global Particle           *particles,
                                       __global LinearSpringForce  *forces,
                                       __global CTriangle          *pTriangles,
                                       __global float              *pDistance,
                                       __global long               *pIndexVertex,
                                                Params              params)
{
	
    long lIndexTrianlge      = -1;
    unsigned int IndexVertex = 0;
    
    float fDistanceClosest = FLT_MAX;
    for(unsigned long ulTriangle=0; ulTriangle<params.CountTriangles; ulTriangle++)
    {
        if(0.0f < pDistance[ulTriangle])
        {
            if(pDistance[ulTriangle] < fDistanceClosest)
            {
                fDistanceClosest = pDistance[ulTriangle];
                lIndexTrianlge     = ulTriangle;
            }
        }
    }

    if((-1 < lIndexTrianlge) && (0.0f <= fDistanceClosest))
    {
        float3 RayEnd   = {params.vecEndRay.x  , params.vecEndRay.y  , params.vecEndRay.z};
        float3 RayStart = {params.vecStartRay.x, params.vecStartRay.y, params.vecStartRay.z};
        
        float3 RayDirNormalize  = normalize(RayEnd - RayStart);

        float3 fIntersectionPoint  = RayStart + (RayDirNormalize * fDistanceClosest);
        
        if(0 > pIndexVertex[pIndexVertex[0]])
        {
            unsigned int IndexOne   = pTriangles[lIndexTrianlge].m_IndexVertices[0];
            unsigned int IndexTwo   = pTriangles[lIndexTrianlge].m_IndexVertices[1];
            unsigned int IndexThree = pTriangles[lIndexTrianlge].m_IndexVertices[2];
        
            float3 VertexOne   = positions[IndexOne].xyz;
            float3 VertexTwo   = positions[IndexTwo].xyz;
            float3 VertexThree = positions[IndexThree].xyz;
        
            float fDisOne   = distance(VertexOne,   fIntersectionPoint);
            float fDisTwo   = distance(VertexTwo,   fIntersectionPoint);
            float fDisThree = distance(VertexThree, fIntersectionPoint);
        
            IndexVertex     = IndexOne;
            float fShortDis = fDisOne;
        
            if(fDisTwo < fShortDis)
            {
                fShortDis   = fDisTwo;
                IndexVertex = IndexTwo;
            }
            if(fDisThree < fShortDis)
            {
                IndexVertex = IndexThree;
            }
        }
        else
        {
            IndexVertex = pIndexVertex[pIndexVertex[0]];
        }
        
        particles[IndexVertex].ppos.x = particles[IndexVertex].pos.x;
        particles[IndexVertex].ppos.y = particles[IndexVertex].pos.y;
        particles[IndexVertex].ppos.z = particles[IndexVertex].pos.z;
        
        particles[IndexVertex].pos.x  = fIntersectionPoint.x;
        particles[IndexVertex].pos.y  = fIntersectionPoint.y;
        particles[IndexVertex].pos.z  = fIntersectionPoint.z;
        
        particles[IndexVertex].fixed  = true;
        
        positions[IndexVertex].x      = fIntersectionPoint.x;
        positions[IndexVertex].y      = fIntersectionPoint.y;
        positions[IndexVertex].z      = fIntersectionPoint.z;
        
        pIndexVertex[pIndexVertex[0]] = IndexVertex;
    }
	
}


            //
            //  UpdateTriangleByDistance
            //
__kernel void MouseEvent(__global float4             *positions,
                         __global Particle           *particles,
                         __global LinearSpringForce  *forces,
                         __global CTriangle          *pTriangles,
                         __global float              *pDistance,
                         __global long               *pIndexVertex,
                                  Params              params)
{
	
    if(LB_UP == params.MouseEvent)
    {

    }
    else if('z' == params.MouseEvent) //(LZ_DOWN == params.MouseEvent)
    {
        long lCount = pIndexVertex[0];
        for(long lIndex=1; lIndex<=lCount; lIndex++)
        {
            particles[pIndexVertex[lIndex]].fixed  = false;
            pIndexVertex[lIndex] = -1;
        }
        pIndexVertex[0] = 1;
    }
    else if('x' == params.MouseEvent) //(LX_DOWN == params.MouseEvent)
    {
         if(0 < pIndexVertex[0])
         {
             pIndexVertex[0]++;
         }
    }
	
}












