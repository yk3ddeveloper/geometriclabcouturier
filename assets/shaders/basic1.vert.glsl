#version 150

in vec4 ciPosition;
out vec4 position;
in vec4 pointsFeatures;
out vec4 pointsFeaturesFrag;
uniform mat4 ciModelViewProjection;

void main() {
	position = ciPosition;
	pointsFeaturesFrag = pointsFeatures;
	gl_Position = ciModelViewProjection * ciPosition;
}