#version 150
in int pointsMarked;
flat out int pointsMarkedFrag;
in vec4 ciPosition;
out vec4 position;
in vec4 pointsFeatures;
out vec4 pointsFeaturesFrag;
uniform mat4 ciModelViewProjection;

//uniform float gl_PointSize;
void main() {
	position = ciPosition;
	pointsMarkedFrag = pointsMarked;
	pointsFeaturesFrag = pointsFeatures;
	gl_Position = ciModelViewProjection * ciPosition;
	//gl_Position.w = 1.0f;

	pointsFeaturesFrag = pointsFeatures;
	gl_PointSize =7.0f;
}