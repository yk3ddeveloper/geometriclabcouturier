#version 150

uniform mat4	ciModelView;
uniform mat4	ciModelViewProjection;
uniform mat3	ciNormalMatrix;

in vec4			ciPosition;
in vec3			ciNormal;
in vec3			ciTangent;

out vec4		VertexViewSpace;
out vec3		NormalViewSpace;

in uint pointsFeatures;
out float pointsFeaturesFrag;
out vec4 tcolor;
void main()
{
	// calculate view space position (required for lighting)
	VertexViewSpace = ciModelView * ciPosition;
	tcolor.r = 0.9f;// clamp(100.0f*pointsFeatures.x, 0.0f, 1.0f);
	tcolor.g = 0.2f;// 0.8*oColor.r;
	tcolor.b = 1.0f - tcolor.r;
	pointsFeaturesFrag = float(pointsFeatures);
	// calculate view space normal (required for lighting & normal mapping)
	NormalViewSpace = normalize(ciNormalMatrix * ciNormal);

	
	pointsFeaturesFrag = pointsFeatures;
	// vertex shader must always pass projection space position
	gl_Position = ciModelViewProjection * ciPosition;
}