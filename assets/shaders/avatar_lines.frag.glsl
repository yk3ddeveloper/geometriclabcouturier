#version 330
uniform float uStepsRun;
//in vec4 forcesFeatures;
in vec4 viewDirection;
in vec4 position;
in vec4 pos;
in vec4 normal;
out vec4 oColor;
uniform vec4 uCameraPosition;


void main() {
	oColor = vec4(1.0f);
	//float dx = 1.0f - clamp(abs(0.4f*forcesFeatures.y),0.0f,1.0f);
	//oColor.b = dx;
	//oColor.r = 1.0f - dx;
	//oColor.g = 0.7f*(1.0f - dx);
	float factor = pow(clamp(0.01f*length(uCameraPosition.xyz - pos.xyz),0.0f,1.0f),4.0f);
	oColor.a = factor*(1.0f-0.4f*pow(abs(dot(viewDirection.xyz,normal.xyz)),6.0f));
	//oColor.b = 0.6f*factor;
	//oColor.r = 1.0f-oColor.b;
}