#version 330
in vec4 position;
in vec4 color;
out vec4 oColor;


void main() {
	oColor = color;
	if (oColor.a < 0.01f) {
		discard;
	}
}