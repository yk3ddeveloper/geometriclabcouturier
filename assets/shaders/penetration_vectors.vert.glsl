#version 150
in vec4 ciPosition;
out vec4 position;
in vec4 ciColor;
out vec4 color;
uniform mat4 ciModelViewProjection;

void main() {
	position = ciPosition;

	color = ciColor;
	gl_Position = ciModelViewProjection * ciPosition;
}