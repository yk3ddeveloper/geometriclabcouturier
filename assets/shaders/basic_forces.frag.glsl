#version 330
uniform float uStepsRun;
uniform float uColors;
uniform float uDrawByType;
in vec4 forcesFeatures;
in vec4 position;
out vec4 oColor;


void main() {
	oColor = vec4(0.0f);
	oColor.a = 1.0f;
	int gColor = int(forcesFeatures.w);
	//float isBackward = float((gColor >> 3) & 1);
	//if (isBackward == 1.0f) {
	//	discard;
	//}
	if (uColors == 1.0f) {
		oColor.g = 1.0f;
	}
	else {
		if (uDrawByType == 1.0f) {
			oColor.r = float((gColor >> 2) & 1);
			oColor.g = float((gColor >> 1) & 1);
			oColor.b = float((gColor >> 0) & 1);
		}
		else {
			float dx = 1.0f - clamp(abs(0.4f*forcesFeatures.y), 0.0f, 1.0f);
			oColor.b = dx;
			oColor.r = 1.0f - dx;
			oColor.g = 0.7f*(1.0f - dx);
		}
		
	}
}