#version 150


//uniform sampler2D	texGarment;
//uniform sampler2D	texGarmentNormal;
uniform vec3		uLightLocViewSpace;
uniform float		uSpecularPower;
// inputs passed from the vertex shader
in vec4		VertexViewSpace;
in vec3     NormalViewSpace;
in vec3		TangentViewSpace;
in vec3		BitangentViewSpace;
in vec2		TexCoord0;
// output a single color
in vec4 tcolor;
in float pointsFeaturesFrag;
out vec4			oColor;

void main()
{
	// fetch the normal from the normal map
	//vec3 vMappedNormal = texture(texGarmentNormal, TexCoord0).rgb * 2.0f - 1.0f;

	// modify it using the normal & tangents from the 3D mesh (normal mapping)
	vec3 normal = NormalViewSpace.xyz;// normalize((TangentViewSpace.xyz * vMappedNormal.x) + (BitangentViewSpace.xyz * vMappedNormal.y) + (NormalViewSpace.xyz * vMappedNormal.z));

	vec3 vToCamera = normalize(-VertexViewSpace.xyz);
	vec3 light = normalize(uLightLocViewSpace.xyz - VertexViewSpace.xyz);
	vec3 reflect = normalize(-reflect(light, normal));

	// calculate diffuse term
	float fDiffuse = max(dot(normal, light), 0.0f);
	fDiffuse = clamp(fDiffuse, 0.5f, 1.0f);

	// calculate specular term
	float fSpecular = pow(max(dot(reflect, vToCamera), 0.0f), uSpecularPower);
	fSpecular = clamp(fSpecular, 0.0f, 1.0f);

	vec3 vDiffuseColor = vec3(fDiffuse) * tcolor.rgb;//* texture( texGarment, TexCoord0 ).rgb;

	vec3 vSpecularColor = vec3(fSpecular);
	if (pointsFeaturesFrag != 1.0f) {
		discard;
	}
	// output colors to buffer
	oColor.rgb = (vDiffuseColor + vSpecularColor).rgb;
	//oColor.rgb = vDiffuseColor.rgb; // texture( texGarment, TexCoord0 ).rgb;
	oColor.a = 1.0f;
	//oColor = vec4(1.0);
}