#version 330
uniform vec3 uBoxSize;
uniform float uColors;
in vec4 position;
out vec4 oColor;
in vec4 pointsFeaturesFrag;
vec3 hsv2rgb(vec3 c)
{
	c.z = abs(c.z);
	vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
	vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
	return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void main() {
	oColor.a = 1.0f;
	if (uColors == 1.0f) {
		oColor.g = 1.0f;
	}
	else {
		//oColor.r = pointsFeaturesFrag.x;
		//oColor.g = 1.0f - pointsFeaturesFrag.x;
		oColor.r =  clamp(100.0f*pointsFeaturesFrag.x,0.0f,1.0f);
		oColor.g = 0.8f;// 0.8*oColor.r;
		oColor.b = 1.0f-oColor.r;
	}
}