#version 150
in vec4 ciPosition;
out vec4 position;
in vec4 forceFeatures;
out vec4 forcesFeatures;
uniform mat4 ciModelViewProjection;

void main() {
	position = ciPosition;

	forcesFeatures = forceFeatures;
	gl_Position = ciModelViewProjection * ciPosition;
}