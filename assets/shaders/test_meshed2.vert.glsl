#version 150

uniform mat4	ciModelView;
uniform mat4	ciModelViewProjection;
uniform mat3	ciNormalMatrix;

in vec4			ciPosition;
in vec3			ciNormal;
in vec3			ciTangent;
in vec2			ciTexCoord0;

out vec4		VertexViewSpace;
out vec3		NormalViewSpace;
out vec3		TangentViewSpace;
out vec3		BitangentViewSpace;
out vec2		TexCoord0;
in vec4 generalPurposeParticleBuffer8;
in vec4 pointsFeatures;
out vec4 pointsFeaturesFrag;
out vec4 tcolor;
void main()
{	
	// calculate view space position (required for lighting)
	VertexViewSpace = ciModelView * ciPosition;
	//tcolor.r = clamp(100.0f*pointsFeatures.x, 0.0f, 1.0f);
	//tcolor.g = 0.8f;// 0.8*oColor.r;
	//tcolor.b = 1.0f - tcolor.r;
	tcolor.a = 1.0;
	vec3 ttcolor = abs(generalPurposeParticleBuffer8.xyz);// abs(clamp(generalPurposeParticleBuffer8.xyz, -1.0f, 1.0f));
	//tt
	if (length(ttcolor.rgb) > 0.0001f) {
		tcolor.rgb = normalize(ttcolor);
	}
	else {
		tcolor.rgb = vec3(0.0f);
	}
	//if ( length(tcolor))
	// calculate view space normal (required for lighting & normal mapping)
	NormalViewSpace = normalize(ciNormalMatrix * ciNormal);

	// calculate tangent and construct the bitangent (required for normal mapping)
	TangentViewSpace = normalize( ciNormalMatrix * ciTangent );
	BitangentViewSpace = normalize( cross( TangentViewSpace, NormalViewSpace ) );
	// pass texture coordinates
	TexCoord0 = ciTexCoord0;
	pointsFeaturesFrag = pointsFeatures;
	// vertex shader must always pass projection space position
	gl_Position = ciModelViewProjection * ciPosition;
}