#version 330
uniform vec3 uBoxSize;
uniform float uColors;
in vec4 position;
out vec4 oColor;
in vec4 pointsFeaturesFrag;
flat in int pointsMarkedFrag;
void main() {
	vec2 coord = gl_PointCoord - vec2(0.5);  //from [0,1] to [-0.5,0.5]

	//outside of circle radius?
	if (pointsMarkedFrag == 0 || length(coord) > 0.5) {
		discard;
	}

	oColor.a = 1.0f;
	if (pointsFeaturesFrag.x == 0) {
		oColor.r = 1.0f;
	}
	else {
		oColor.b = 1.0f;

	}
}