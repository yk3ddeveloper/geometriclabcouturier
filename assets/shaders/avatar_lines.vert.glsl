#version 150
uniform vec4 uCameraPosition;
in vec4 ciPosition;
in vec4 ciNormal;
out vec4 position;
out vec4 pos;
out vec4 viewDirection;
out vec4 normal;
//in vec4 forceFeatures;
//out vec4 forcesFeatures;
uniform mat4 ciModelViewProjection;
uniform mat4 ciModelView;

void main() {
	position = ciPosition*ciModelViewProjection;
	pos = ciPosition;
	normal = ciNormal*ciModelView;
	viewDirection = normalize(uCameraPosition - ciPosition)*ciModelView;
	//forcesFeatures = forceFeatures;
	gl_Position = ciModelViewProjection * ciPosition;
}